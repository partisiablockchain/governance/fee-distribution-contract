package com.partisiablockchain.governance.feedistribution;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.SafeDataOutputStream;

final class AccountPluginRpc {
  static final byte ADD_COIN_BALANCE_INVOCATION_BYTE = 0;
  static final byte COLLECT_SERVICE_AND_INFRASTRUCTURE_FEES_FOR_DISTRIBUTION_INVOCATION_BYTE = 20;

  @SuppressWarnings("unused")
  private AccountPluginRpc() {}

  static byte[] collectServiceAndInfrastructureFeesForDistribution(long epoch) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(
              COLLECT_SERVICE_AND_INFRASTRUCTURE_FEES_FOR_DISTRIBUTION_INVOCATION_BYTE);
          stream.writeLong(epoch);
        });
  }

  static byte[] addCoinBalance(CoinToPay coinToPay) {
    return SafeDataOutputStream.serialize(
        s -> {
          s.writeByte(ADD_COIN_BALANCE_INVOCATION_BYTE);
          s.writeString(coinToPay.getCoinSymbol());
          coinToPay.getCoins().write(s);
        });
  }
}
