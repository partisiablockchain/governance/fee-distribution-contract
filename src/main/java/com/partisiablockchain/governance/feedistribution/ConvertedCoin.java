package com.partisiablockchain.governance.feedistribution;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.util.ListUtil;

/** Mimics a converted external coin from account plugin. */
@Immutable
public final class ConvertedCoin implements StateSerializable {

  static final ListUtil<ConvertedCoin> COINS_LIST =
      new ListUtil<>(ConvertedCoin::create, ConvertedCoin::isEmpty);

  private final Unsigned256 convertedCoins;
  private final Unsigned256 convertedGasSum;

  /** Serializable constructor. */
  @SuppressWarnings("unused")
  public ConvertedCoin() {
    convertedCoins = null;
    convertedGasSum = null;
  }

  /**
   * Default constructor.
   *
   * @param convertedCoins coins converted
   * @param convertedGasSum sum of gas converted
   */
  public ConvertedCoin(Unsigned256 convertedCoins, Unsigned256 convertedGasSum) {
    this.convertedCoins = convertedCoins;
    this.convertedGasSum = convertedGasSum;
  }

  /**
   * Get amount of converted coins.
   *
   * @return amount of converted coins
   */
  public Unsigned256 getConvertedCoins() {
    return convertedCoins;
  }

  /**
   * Add converted coins.
   *
   * @param convertedCoins amount of coins converted
   * @param convertedGasSum sum of gas converted
   * @return next converted coin state
   */
  public ConvertedCoin addConvertedCoins(Unsigned256 convertedCoins, Unsigned256 convertedGasSum) {
    return new ConvertedCoin(
        this.convertedCoins.add(convertedCoins), this.convertedGasSum.add(convertedGasSum));
  }

  /**
   * Deduct paid out converted coins.
   *
   * @param convertedCoins amount of coins paid
   * @param convertedGasSum sum of gas paid
   * @return next converted coin state
   */
  public ConvertedCoin deductConvertedCoins(
      Unsigned256 convertedCoins, Unsigned256 convertedGasSum) {
    return new ConvertedCoin(
        this.convertedCoins.subtract(convertedCoins),
        this.convertedGasSum.subtract(convertedGasSum));
  }

  /**
   * Get sum of gas converted .
   *
   * @return sum of gas converted
   */
  public Unsigned256 getConvertedGasSum() {
    return convertedGasSum;
  }

  /**
   * Create converted coin state.
   *
   * @return created converted coin state
   */
  public static ConvertedCoin create() {
    return new ConvertedCoin(Unsigned256.ZERO, Unsigned256.ZERO);
  }

  boolean isEmpty() {
    return this.getConvertedCoins().equals(Unsigned256.ZERO);
  }

  static ConvertedCoin createFromStateAccessor(StateAccessor stateAccessor) {
    Unsigned256 convertedCoins = stateAccessor.get("convertedCoins").cast(Unsigned256.class);
    Unsigned256 convertedGasSum = stateAccessor.get("convertedGasSum").cast(Unsigned256.class);

    return new ConvertedCoin(convertedCoins, convertedGasSum);
  }
}
