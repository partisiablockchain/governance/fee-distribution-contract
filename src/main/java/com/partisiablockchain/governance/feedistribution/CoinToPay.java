package com.partisiablockchain.governance.feedistribution;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.math.Unsigned256;

final class CoinToPay {

  private final int coinIndex;
  private final String coinSymbol;
  private final Unsigned256 coins;
  private final Unsigned256 gasEquivalent;

  CoinToPay(int coinIndex, String coinSymbol, Unsigned256 coins, Unsigned256 gasEquivalent) {
    this.coinIndex = coinIndex;
    this.coinSymbol = coinSymbol;
    this.coins = coins;
    this.gasEquivalent = gasEquivalent;
  }

  public String getCoinSymbol() {
    return coinSymbol;
  }

  public Unsigned256 getCoins() {
    return coins;
  }

  public Unsigned256 getGasEquivalent() {
    return gasEquivalent;
  }

  public int getCoinIndex() {
    return coinIndex;
  }
}
