package com.partisiablockchain.governance.feedistribution;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateAccessorAvlLeafNode;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.util.List;

@Immutable
final class ShardInformation implements StateSerializable {

  private final FixedList<BlockchainAddress> notifyingBlockProducers;
  private final AvlTree<BlockchainAddress, EpochMetrics> metrics;

  @SuppressWarnings("unused")
  private ShardInformation() {
    notifyingBlockProducers = null;
    metrics = null;
  }

  ShardInformation(
      FixedList<BlockchainAddress> notifyingBlockProducers,
      AvlTree<BlockchainAddress, EpochMetrics> metrics) {
    this.notifyingBlockProducers = notifyingBlockProducers;
    this.metrics = metrics;
  }

  static ShardInformation initial() {
    return new ShardInformation(FixedList.create(), AvlTree.create());
  }

  public AvlTree<BlockchainAddress, EpochMetrics> getMetrics() {
    return metrics;
  }

  public ShardInformation registerEpochEnded(
      BlockchainAddress blockProducer, EpochMetrics reportedMetrics) {
    FixedList<BlockchainAddress> updatedNotifyingBlockProducers;
    AvlTree<BlockchainAddress, EpochMetrics> updatedMetrics;
    if (!notifyingBlockProducers.contains(blockProducer)) {
      updatedNotifyingBlockProducers = notifyingBlockProducers.addElement(blockProducer);
      updatedMetrics = metrics.set(blockProducer, reportedMetrics);
    } else {
      updatedNotifyingBlockProducers = notifyingBlockProducers;
      updatedMetrics = metrics;
    }
    return new ShardInformation(updatedNotifyingBlockProducers, updatedMetrics);
  }

  static ShardInformation createFromStateAccessor(StateAccessor accessor) {
    FixedList<BlockchainAddress> notifyingBlockProducers =
        accessor.get("notifyingBlockProducers").typedFixedList(BlockchainAddress.class);
    AvlTree<BlockchainAddress, EpochMetrics> metrics = AvlTree.create();
    for (StateAccessorAvlLeafNode leaf : accessor.get("metrics").getTreeLeaves()) {
      BlockchainAddress key = leaf.getKey().typedValue(BlockchainAddress.class);
      EpochMetrics value = EpochMetrics.createFromStateAccessor(leaf.getValue());
      metrics = metrics.set(key, value);
    }
    return new ShardInformation(notifyingBlockProducers, metrics);
  }

  /**
   * Returns a list of producers who was in the top 2t+1 of at least t+1 other producers.
   *
   * @param blockProducers the list of all block producers
   * @return a list of good producers according to the reported metrics.
   */
  FixedList<BlockchainAddress> filterByReportedMetrics(
      FixedList<BlockchainAddress> blockProducers) {
    int[] metricCounts = new int[blockProducers.size()];
    int threshold = (blockProducers.size() - 1) / 3;
    for (BlockchainAddress notifyingBlockProducer : notifyingBlockProducers) {
      EpochMetrics metric = metrics.getValue(notifyingBlockProducer);
      List<Integer> goodIndices = metric.getGoodIndices(threshold);
      for (Integer goodIndex : goodIndices) {
        if (goodIndex < blockProducers.size()) {
          metricCounts[goodIndex]++;
        }
      }
    }

    FixedList<BlockchainAddress> payees = FixedList.create();
    for (int i = 0; i < blockProducers.size(); i++) {
      BlockchainAddress blockProducer = blockProducers.get(i);
      if (metricCounts[i] > threshold && notifyingBlockProducers.contains(blockProducer)) {
        payees = payees.addElement(blockProducer);
      }
    }

    if (payees.isEmpty()) {
      return notifyingBlockProducers;
    } else {
      return payees;
    }
  }
}
