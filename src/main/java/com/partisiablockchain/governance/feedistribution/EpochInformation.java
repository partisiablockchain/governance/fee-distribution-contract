package com.partisiablockchain.governance.feedistribution;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateAccessorAvlLeafNode;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;

/** Object for retrieving information of shards for an epoch. */
@Immutable
public final class EpochInformation implements StateSerializable {

  private final AvlTree<ShardId, ShardInformation> shards;

  @SuppressWarnings("unused")
  private EpochInformation() {
    shards = null;
  }

  EpochInformation(AvlTree<ShardId, ShardInformation> shards) {
    this.shards = shards;
  }

  static EpochInformation initial() {
    return new EpochInformation(AvlTree.create());
  }

  /**
   * Get the list of block producers who (1) appear good, and (2) has notified that this epoch ended
   * for the given shard. This method returns null if the provided ShardId maps to null.
   *
   * @param shard the shard
   * @param blockProducers the list of all block producers
   * @return a list of good notifying block producers for this shard.
   */
  FixedList<BlockchainAddress> getBlockProducersForShardBasedOnMetrics(
      ShardId shard, FixedList<BlockchainAddress> blockProducers) {
    ShardInformation value = shards.getValue(shard);
    if (value == null) {
      return null;
    } else {
      return value.filterByReportedMetrics(blockProducers);
    }
  }

  AvlTree<ShardId, ShardInformation> getShards() {
    return shards;
  }

  /**
   * Register an 'epoch ended' message from a block producer.
   *
   * @param blockProducer the notifying block producer
   * @param shard the shard of the block producer
   * @param metrics metrics reported by the notifying block producer
   * @return the updated state
   */
  public EpochInformation registerEpochEnded(
      BlockchainAddress blockProducer, ShardId shard, EpochMetrics metrics) {
    ShardInformation current;
    if (shards.containsKey(shard)) {
      current = shards.getValue(shard);
    } else {
      current = ShardInformation.initial();
    }
    return new EpochInformation(
        shards.set(shard, current.registerEpochEnded(blockProducer, metrics)));
  }

  static EpochInformation createFromStateAccessor(StateAccessor accessor) {
    AvlTree<ShardId, ShardInformation> shardsMap = AvlTree.create();
    for (StateAccessorAvlLeafNode entry : accessor.get("shards").getTreeLeaves()) {
      ShardId shardId = ShardId.createFromStateAccessor(entry.getKey());
      ShardInformation shardInformation =
          ShardInformation.createFromStateAccessor(entry.getValue());
      shardsMap = shardsMap.set(shardId, shardInformation);
    }
    return new EpochInformation(shardsMap);
  }
}
