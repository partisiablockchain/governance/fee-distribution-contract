package com.partisiablockchain.governance.feedistribution;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializableInline;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Contains information about the frequency of signatures received from other block producers in an
 * epoch.
 */
@Immutable
public final class EpochMetrics implements StateSerializableInline {

  private final FixedList<Integer> signatureFrequencies;

  @SuppressWarnings("unused")
  EpochMetrics() {
    signatureFrequencies = null;
  }

  private EpochMetrics(FixedList<Integer> signatureFrequencies) {
    this.signatureFrequencies = signatureFrequencies;
  }

  /**
   * Create a new Epoch metrics object.
   *
   * @param signatureFrequencies signature frequencies
   */
  public EpochMetrics(List<Integer> signatureFrequencies) {
    this(FixedList.create(signatureFrequencies));
  }

  static EpochMetrics createFromStateAccessor(StateAccessor value) {
    return new EpochMetrics(value.get("signatureFrequencies").typedFixedList(Integer.class));
  }

  FixedList<Integer> getSignatureFrequencies() {
    return signatureFrequencies;
  }

  List<Integer> getGoodIndices(int threshold) {
    if (signatureFrequencies.size() <= 2 * threshold) {
      return List.of();
    } else {
      ArrayList<Integer> integers = new ArrayList<>(signatureFrequencies);
      integers.sort(Collections.reverseOrder());
      List<Integer> indices = new ArrayList<>();
      Integer cutoff = integers.get(2 * threshold);
      for (int i = 0; i < signatureFrequencies.size(); i++) {
        Integer freq = signatureFrequencies.get(i);
        if (freq >= cutoff) {
          indices.add(i);
        }
      }
      return indices;
    }
  }
}
