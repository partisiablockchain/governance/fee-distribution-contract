package com.partisiablockchain.governance.feedistribution;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.contract.sys.SystemEventCreator;
import com.partisiablockchain.contract.sys.SystemEventManager;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.partisiablockchain.util.Unsigned256Util;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/** State for the {@link FeeDistributionContract}. */
@Immutable
public final class FeeDistributionContractState implements StateSerializable {

  static final long PAYOUT_THRESHOLD = 1_000_000;
  private final BlockchainAddress bpOrchestrationContract;
  private final FixedList<BlockchainAddress> blockProducers;
  private final FixedList<ConvertedCoin> externalCoins;
  private final AvlTree<BlockchainAddress, Unsigned256> feesToDistribute;
  private final AvlTree<Long, EpochInformation> epochs;
  private final long lastEpochCollected;
  private final FixedList<ShardId> shardIds;
  private final Signed256 gasRewardPool;

  private static final Unsigned256 HUNDRED = Unsigned256.create(100);
  private static final Unsigned256 TAX_PERCENTAGE = Unsigned256.create(5);

  /**
   * Create fee distribution contract state.
   *
   * @param bpOrchestrationContract the address of the bp orchestration contract
   * @param blockProducers addresses of block producers
   * @param shards ids of shard
   * @return next state of contract
   */
  public static FeeDistributionContractState initial(
      BlockchainAddress bpOrchestrationContract,
      FixedList<BlockchainAddress> blockProducers,
      FixedList<ShardId> shards) {
    return new FeeDistributionContractState(
        bpOrchestrationContract,
        blockProducers,
        FixedList.create(),
        AvlTree.create(),
        AvlTree.create(),
        -1,
        shards,
        Signed256.create());
  }

  /**
   * Get the address of the block producer orchestration contract.
   *
   * @return the contract address
   */
  public BlockchainAddress getBpOrchestrationContract() {
    return bpOrchestrationContract;
  }

  /**
   * Get information about an epoch containing the notifying block producers for each shard and if
   * rewards has been collected.
   *
   * @param epoch the epoch to get information for
   * @return the information about the epoch
   */
  public EpochInformation getEpochShardInformation(Long epoch) {
    if (epochs.containsKey(epoch)) {
      return epochs.getValue(epoch);
    } else {
      return EpochInformation.initial();
    }
  }

  @SuppressWarnings("unused")
  private FeeDistributionContractState() {
    blockProducers = null;
    externalCoins = null;
    feesToDistribute = null;
    epochs = null;
    shardIds = null;
    gasRewardPool = null;
    bpOrchestrationContract = null;
    lastEpochCollected = 0;
  }

  FeeDistributionContractState(
      BlockchainAddress bpOrchestrationContract,
      FixedList<BlockchainAddress> blockProducers,
      FixedList<ConvertedCoin> externalCoins,
      AvlTree<BlockchainAddress, Unsigned256> feesToDistribute,
      AvlTree<Long, EpochInformation> epochs,
      long lastEpochCollected,
      FixedList<ShardId> shardIds,
      Signed256 gasRewardPool) {
    this.bpOrchestrationContract = bpOrchestrationContract;
    this.blockProducers = blockProducers;
    this.externalCoins = externalCoins;
    this.feesToDistribute = feesToDistribute;
    this.epochs = epochs;
    this.lastEpochCollected = lastEpochCollected;
    this.shardIds = shardIds;
    this.gasRewardPool = gasRewardPool;
  }

  /**
   * Get block producers.
   *
   * @return list of addresses of block producers
   */
  public FixedList<BlockchainAddress> getBlockProducers() {
    return blockProducers;
  }

  /**
   * Get shard ids.
   *
   * @return list of shard ids
   */
  public FixedList<ShardId> getShardIds() {
    return shardIds;
  }

  /**
   * Get the gas reward pool.
   *
   * @return the sum of the gas reward pool
   */
  public Signed256 getGasRewardPool() {
    return gasRewardPool;
  }

  /**
   * Get external coins.
   *
   * @return list of converted coin states
   */
  public FixedList<ConvertedCoin> getExternalCoins() {
    return externalCoins;
  }

  /**
   * Get fees to distribute.
   *
   * @return map from fee receiver address to fee amount
   */
  public AvlTree<BlockchainAddress, Unsigned256> getFeesToDistribute() {
    return feesToDistribute;
  }

  FeeDistributionContractState updatedExternalCoins(FixedList<ConvertedCoin> updatedCoins) {
    return new FeeDistributionContractState(
        bpOrchestrationContract,
        blockProducers,
        updatedCoins,
        feesToDistribute,
        epochs,
        lastEpochCollected,
        shardIds,
        gasRewardPool);
  }

  FeeDistributionContractState updatedFeesToDistribute(
      AvlTree<BlockchainAddress, Unsigned256> updatedFees) {
    return new FeeDistributionContractState(
        bpOrchestrationContract,
        blockProducers,
        externalCoins,
        updatedFees,
        epochs,
        lastEpochCollected,
        shardIds,
        gasRewardPool);
  }

  FeeDistributionContractState updatedRewardPool(Signed256 updatedRewardPool) {
    return new FeeDistributionContractState(
        bpOrchestrationContract,
        blockProducers,
        externalCoins,
        feesToDistribute,
        epochs,
        lastEpochCollected,
        shardIds,
        updatedRewardPool);
  }

  static DetermineCoinsResult determineCoinsToPay(
      Unsigned256 remainingGasToPay,
      int preferredCoinIndex,
      AccountStateHelper global,
      FixedList<ConvertedCoin> externalCoins) {
    List<CoinToPay> coinList = new ArrayList<>();
    for (ConvertedCoin coin : externalCoins) {
      int i = externalCoins.indexOf(coin);
      int wrapIndex = (i + preferredCoinIndex) % externalCoins.size();
      ConvertedCoin externalCoin = externalCoins.get(wrapIndex);
      if (!externalCoin.isEmpty()) {
        Unsigned256 availableCoins = externalCoins.get(wrapIndex).getConvertedCoins();
        Unsigned256 gasToConvert =
            Unsigned256Util.min(remainingGasToPay, gasEquivalent(externalCoin, availableCoins));
        Unsigned256 coinsToPay = coinEquivalent(externalCoin, gasToConvert);
        String coinSymbol = global.coinSymbols().get(wrapIndex);
        Unsigned256 gasToPay = gasEquivalent(externalCoin, coinsToPay);
        remainingGasToPay = remainingGasToPay.subtract(gasToPay);
        coinList.add(new CoinToPay(wrapIndex, coinSymbol, coinsToPay, gasToPay));
      }
    }
    return new DetermineCoinsResult(coinList, remainingGasToPay);
  }

  private static Unsigned256 coinEquivalent(ConvertedCoin externalCoin, Unsigned256 gasToConvert) {
    return gasToConvert
        .multiply(externalCoin.getConvertedCoins())
        .divide(externalCoin.getConvertedGasSum());
  }

  private static Unsigned256 gasEquivalent(ConvertedCoin externalCoin, Unsigned256 availableCoins) {
    return Unsigned256Util.ceilDiv(
        availableCoins.multiply(externalCoin.getConvertedGasSum()),
        externalCoin.getConvertedCoins());
  }

  FeeDistributionContractState storeCollectedFees(
      CallbackContext.ExecutionResult shardResult, ShardId shardId) {
    FixedList<ConvertedCoin> updatedExternalCoins = getExternalCoins();
    AvlTree<BlockchainAddress, Unsigned256> updatedFeesToDistribute = getFeesToDistribute();

    SafeDataInputStream returnValue = shardResult.returnValue();

    long noConvertedCoins = returnValue.readLong();
    for (int i = 0; i < noConvertedCoins; i++) {
      Unsigned256 convertedCoins = Unsigned256.read(returnValue);
      Unsigned256 convertedGasSum = Unsigned256.read(returnValue);
      updatedExternalCoins =
          ConvertedCoin.COINS_LIST.withUpdatedEntry(
              updatedExternalCoins,
              i,
              convertedCoin -> convertedCoin.addConvertedCoins(convertedCoins, convertedGasSum));
    }

    long blockchainUsageTreeSize = returnValue.readLong();
    Signed256 updatedRewardPool = getGasRewardPool();
    for (int i = 0; i < blockchainUsageTreeSize; i++) {
      long epoch = returnValue.readLong();
      Unsigned256 blockchainUsage = Unsigned256.read(returnValue);
      Unsigned256 taxCut = calculatePercentage(blockchainUsage, TAX_PERCENTAGE);
      blockchainUsage = blockchainUsage.subtract(taxCut);
      updatedRewardPool = updatedRewardPool.add(taxCut);
      FixedList<BlockchainAddress> receivingBlockProducers = null;
      if (Objects.requireNonNull(epochs).containsKey(epoch)) {
        EpochInformation epochInformation = epochs.getValue(epoch);
        receivingBlockProducers =
            epochInformation.getBlockProducersForShardBasedOnMetrics(shardId, blockProducers);
      }
      receivingBlockProducers = Objects.requireNonNullElse(receivingBlockProducers, blockProducers);

      AvlTree<BlockchainAddress, Unsigned256> distributedGas =
          evenlyDistribute(blockchainUsage, Objects.requireNonNull(receivingBlockProducers));
      for (BlockchainAddress blockProducer : receivingBlockProducers) {
        Unsigned256 current = getCurrentFee(updatedFeesToDistribute, blockProducer);
        Unsigned256 updated = current.add(distributedGas.getValue(blockProducer));
        updatedFeesToDistribute = updatedFeesToDistribute.set(blockProducer, updated);
      }
    }

    long noRegisteredFees = returnValue.readLong();
    for (int i = 0; i < noRegisteredFees; i++) {
      BlockchainAddress blockchainAddress = BlockchainAddress.read(returnValue);
      Unsigned256 current = getCurrentFee(updatedFeesToDistribute, blockchainAddress);
      Unsigned256 registeredFees = Unsigned256.read(returnValue);
      Unsigned256 taxCut = calculatePercentage(registeredFees, TAX_PERCENTAGE);
      registeredFees = registeredFees.subtract(taxCut);
      updatedRewardPool = updatedRewardPool.add(taxCut);
      Unsigned256 updated = current.add(registeredFees);
      updatedFeesToDistribute = updatedFeesToDistribute.set(blockchainAddress, updated);
    }

    Unsigned256 infrastructureFeeSum = Unsigned256.read(returnValue);
    updatedRewardPool = updatedRewardPool.subtract(infrastructureFeeSum);

    return updatedExternalCoins(updatedExternalCoins)
        .updatedFeesToDistribute(updatedFeesToDistribute)
        .updatedRewardPool(updatedRewardPool);
  }

  /**
   * Removes ShardInformation from currentEpoch and all previous epochs.
   *
   * @param currentEpoch the current epoch.
   * @return a new FeeDistributionContractState with updated epochs.
   */
  FeeDistributionContractState cleanEpochInformation(long currentEpoch) {
    AvlTree<Long, EpochInformation> updatedEpochs = epochs;
    for (long epoch : epochs.keySet()) {
      if (epoch <= currentEpoch) {
        updatedEpochs = updatedEpochs.remove(epoch);
      }
    }

    return new FeeDistributionContractState(
        bpOrchestrationContract,
        blockProducers,
        externalCoins,
        feesToDistribute,
        updatedEpochs,
        lastEpochCollected,
        shardIds,
        gasRewardPool);
  }

  private Unsigned256 getCurrentFee(
      AvlTree<BlockchainAddress, Unsigned256> feesToDistribute,
      BlockchainAddress blockchainAddress) {
    return feesToDistribute.containsKey(blockchainAddress)
        ? feesToDistribute.getValue(blockchainAddress)
        : Unsigned256.ZERO;
  }

  /**
   * Get an even distribution of an amount of gas between block producers. If an exactly even
   * distribution can't be achieved, we distribute the remaining as good as possible starting from
   * the lowest index.
   *
   * @param totalGas the amount of gas to be distributed
   * @param blockProducers the block producers to distribute the gas to
   * @return the distribution of gas
   */
  private AvlTree<BlockchainAddress, Unsigned256> evenlyDistribute(
      Unsigned256 totalGas, FixedList<BlockchainAddress> blockProducers) {
    AvlTree<BlockchainAddress, Unsigned256> gasDistribution = AvlTree.create();
    Unsigned256 size = Unsigned256.create(blockProducers.size());
    Unsigned256 gasPerProducer = totalGas.divide(size);
    Unsigned256 remaining = totalGas.subtract(gasPerProducer.multiply(size));
    for (BlockchainAddress producer : blockProducers) {
      int producerIndex = blockProducers.indexOf(producer);

      Unsigned256 distributedGas;
      if (Unsigned256.create(producerIndex).compareTo(remaining) < 0) {
        distributedGas = gasPerProducer.add(Unsigned256.ONE);
      } else {
        distributedGas = gasPerProducer;
      }
      gasDistribution = gasDistribution.set(producer, distributedGas);
    }
    return gasDistribution;
  }

  /**
   * Updates the committee to the parameter list.
   *
   * @param newCommittee the new comittee for the contract
   * @return the new FeeDistributionContractState with the updated committee
   */
  public FeeDistributionContractState updateCommittee(FixedList<BlockchainAddress> newCommittee) {
    return new FeeDistributionContractState(
        bpOrchestrationContract,
        newCommittee,
        externalCoins,
        feesToDistribute,
        epochs,
        lastEpochCollected,
        shardIds,
        gasRewardPool);
  }

  /**
   * Notifies the state that an epoch has ended for a shard.
   *
   * @param notifyingBlockProducer the notifying block producer
   * @param shard the shard
   * @param epoch the epoch
   * @param metrics metrics reported by the notifying block producer
   * @return updated state
   */
  public FeeDistributionContractState registerEpochEnded(
      BlockchainAddress notifyingBlockProducer, ShardId shard, long epoch, EpochMetrics metrics) {
    EpochInformation currentEpochInformation =
        getEpochShardInformation(epoch).registerEpochEnded(notifyingBlockProducer, shard, metrics);

    return new FeeDistributionContractState(
        bpOrchestrationContract,
        blockProducers,
        externalCoins,
        feesToDistribute,
        epochs.set(epoch, currentEpochInformation),
        lastEpochCollected,
        shardIds,
        gasRewardPool);
  }

  /**
   * Check if it is time to pay out collected fees for block producers. If the collected fees for a
   * block producer has exceeded PAYOUT_THRESHOLD, we pay the block producer all collected fees.
   *
   * @param context system contract context for sending events
   * @return updated state
   */
  public FeeDistributionContractState payBlockProducers(SysContractContext context) {
    AvlTree<BlockchainAddress, Unsigned256> updatedFeesToDistribute = getFeesToDistribute();
    FixedList<ConvertedCoin> updatedExternalCoins = getExternalCoins();

    for (BlockchainAddress blockProducer : getFeesToDistribute().keySet()) {
      Unsigned256 gasToPay = getFeesToDistribute().getValue(blockProducer);
      if (gasToPay.compareTo(Unsigned256.create(PAYOUT_THRESHOLD)) >= 0) {
        AccountStateHelper global = new AccountStateHelper(context.getGlobalAccountPluginState());
        DetermineCoinsResult result =
            determineCoinsToPay(gasToPay, 0, global, updatedExternalCoins);
        List<CoinToPay> coinsToPay = result.getCoinsToPay();
        for (CoinToPay coinToPay : coinsToPay) {
          updatedExternalCoins =
              ConvertedCoin.COINS_LIST.withUpdatedEntry(
                  updatedExternalCoins,
                  coinToPay.getCoinIndex(),
                  extCoin ->
                      extCoin.deductConvertedCoins(
                          coinToPay.getCoins(), coinToPay.getGasEquivalent()));

          LocalPluginStateUpdate localUpdate =
              LocalPluginStateUpdate.create(
                  blockProducer, AccountPluginRpc.addCoinBalance(coinToPay));
          context.getInvocationCreator().updateLocalAccountPluginState(localUpdate);
        }
        if (result.getRemaining().equals(Unsigned256.ZERO)) {
          updatedFeesToDistribute = updatedFeesToDistribute.remove(blockProducer);
        } else {
          updatedFeesToDistribute =
              updatedFeesToDistribute.set(blockProducer, result.getRemaining());
        }
      }
    }

    return updatedFeesToDistribute(updatedFeesToDistribute)
        .updatedExternalCoins(updatedExternalCoins);
  }

  /**
   * Confirm an epoch as ended and collect rewards from all shards.
   *
   * @param epoch the epoch
   * @param context the system contract context used to send events to account plugins on shards
   * @return the updated state
   */
  public FeeDistributionContractState confirmEpochEnded(long epoch, SysContractContext context) {
    long updatedLastCollectedEpoch = lastEpochCollected;
    if (epoch > lastEpochCollected) {
      collectRewardsFromAllShards(epoch, context);
      updatedLastCollectedEpoch = epoch;
    }
    return new FeeDistributionContractState(
        bpOrchestrationContract,
        blockProducers,
        externalCoins,
        feesToDistribute,
        epochs,
        updatedLastCollectedEpoch,
        shardIds,
        gasRewardPool);
  }

  private void collectRewardsFromAllShards(long epoch, SysContractContext context) {
    SystemEventManager eventManager = context.getRemoteCallsCreator();
    for (ShardId shardId : getShardIds()) {
      collectRewardsForEpochAndPrevious(shardId, epoch, eventManager);
    }
    eventManager.registerCallbackWithCostFromRemaining(
        FeeDistributionContract.Callbacks.collectRewards(epoch, getShardIds()));
  }

  private void collectRewardsForEpochAndPrevious(
      ShardId shardId, long epoch, SystemEventCreator eventManager) {
    eventManager.updateContextFreeAccountPluginState(
        shardId.shardId(),
        AccountPluginRpc.collectServiceAndInfrastructureFeesForDistribution(epoch));
  }

  private Unsigned256 calculatePercentage(Unsigned256 value, Unsigned256 percentage) {
    return value.multiply(percentage).divide(HUNDRED);
  }
}
