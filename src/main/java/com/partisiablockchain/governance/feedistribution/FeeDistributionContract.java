package com.partisiablockchain.governance.feedistribution;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Callback;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateAccessorAvlLeafNode;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.stream.SafeListStream;
import com.secata.tools.immutable.FixedList;
import java.util.List;

/** A fee distribution system contract. */
@AutoSysContract(FeeDistributionContractState.class)
public final class FeeDistributionContract {

  static final class Invocations {

    static final int NOTIFY = 4;
    static final int UPDATE_COMMITTEE = 1;

    private Invocations() {}
  }

  static final class Callbacks {

    static final int COLLECT_REWARDS = 1;

    private Callbacks() {}

    static DataStreamSerializable collectRewards(long epoch, FixedList<ShardId> shardIds) {
      return stream -> {
        stream.writeByte(FeeDistributionContract.Callbacks.COLLECT_REWARDS);
        stream.writeLong(epoch);
        for (ShardId shardId : shardIds) {
          stream.writeOptional(
              SafeListStream.primitive(SafeDataOutputStream::writeString), shardId.shardId());
        }
      };
    }
  }

  /**
   * Initialize fee distribution contract.
   *
   * @param bpOrchestrationContract bp orchestration contract address
   * @param shards list of shards
   * @return initial state
   */
  @Init
  public FeeDistributionContractState create(
      BlockchainAddress bpOrchestrationContract, List<ShardId> shards) {
    return FeeDistributionContractState.initial(
        bpOrchestrationContract, FixedList.create(), FixedList.create(shards));
  }

  /**
   * Migrate an existing contract to this version.
   *
   * @param oldState accessor for the old state
   * @return the migrated state
   */
  @Upgrade
  public FeeDistributionContractState upgrade(StateAccessor oldState) {
    BlockchainAddress bpOrchestrationAddress =
        oldState.get("bpOrchestrationContract").blockchainAddressValue();

    FixedList<BlockchainAddress> blockProducers =
        oldState.get("blockProducers").typedFixedList(BlockchainAddress.class);

    FixedList<ConvertedCoin> externalCoins =
        FixedList.create(
            oldState.get("externalCoins").getListElements().stream()
                .map(ConvertedCoin::createFromStateAccessor)
                .toList());

    AvlTree<BlockchainAddress, Unsigned256> feesToDistribute =
        oldState.get("feesToDistribute").typedAvlTree(BlockchainAddress.class, Unsigned256.class);

    AvlTree<Long, EpochInformation> epochs = createEpochsTree(oldState.get("epochs"));

    long lastEpochCollected = oldState.get("lastEpochCollected").longValue();

    FixedList<ShardId> shardIds =
        FixedList.create(
            oldState.get("shardIds").getListElements().stream()
                .map(ShardId::createFromStateAccessor)
                .toList());

    Signed256 gasRewardPool = Signed256.createFromStateAccessor(oldState.get("gasRewardPool"));

    return new FeeDistributionContractState(
        bpOrchestrationAddress,
        blockProducers,
        externalCoins,
        feesToDistribute,
        epochs,
        lastEpochCollected,
        shardIds,
        gasRewardPool);
  }

  private static AvlTree<Long, EpochInformation> createEpochsTree(StateAccessor accessor) {
    AvlTree<Long, EpochInformation> epochsTree = AvlTree.create();
    for (StateAccessorAvlLeafNode entry : accessor.getTreeLeaves()) {
      Long epoch = entry.getKey().longValue();
      EpochInformation epochInformation =
          EpochInformation.createFromStateAccessor(entry.getValue());
      epochsTree = epochsTree.set(epoch, epochInformation);
    }
    return epochsTree;
  }

  /**
   * Notify an epoch has ended.
   *
   * <p>Notifications can only be made for the previous epoch, and only block producers in the
   * current committee can notify. When notifying an ended epoch the block producers include metrics
   * for the epoch.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param shardId the shard id for notification
   * @param endedEpoch the ended epoch
   * @param metrics metrics for the epoch
   * @return updated state
   */
  @Action(Invocations.NOTIFY)
  public FeeDistributionContractState notify(
      SysContractContext context,
      FeeDistributionContractState state,
      ShardId shardId,
      long endedEpoch,
      EpochMetricsRpc metrics) {
    BlockchainAddress from = context.getFrom();
    ensure(
        state.getBlockProducers().contains(from),
        "Only block producers can notify an epoch has ended");

    ensure(
        state.getShardIds().contains(shardId),
        "The provided shard does not correspond to a known shard");

    long blockProductionTime = context.getBlockProductionTime();
    long currentEpoch = convertBlockProductionTimeToEpoch(blockProductionTime);
    ensure(
        currentEpoch == endedEpoch + 1,
        "It is only allowed to notify an epoch has ended in the subsequent epoch");

    EpochMetrics epochMetrics = metrics.convert();

    return state
        .registerEpochEnded(from, shardId, endedEpoch, epochMetrics)
        .confirmEpochEnded(currentEpoch - 2, context);
  }

  /**
   * Update the current committee. Only the BP orchestration contract can update the committee.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param newBlockProducers the new committee
   * @return updated state
   */
  @Action(Invocations.UPDATE_COMMITTEE)
  public FeeDistributionContractState updateCommittee(
      SysContractContext context,
      FeeDistributionContractState state,
      List<BlockchainAddress> newBlockProducers) {
    ensure(
        context.getFrom().equals(state.getBpOrchestrationContract()),
        "Only the BP orchestration contract can update the committee");
    ensure(
        newBlockProducers.size() > 0,
        "A fee distribution contract cannot be updated without block producers");
    return state.updateCommittee(FixedList.create(newBlockProducers));
  }

  /**
   * Collects rewards for the given epochs on the shards written to the rpc.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param callbackContext context containing information about callbacks
   * @param epoch the epoch to collect rewards for
   * @param rpc rpc containing a dynamic list of shard IDs
   * @return updated state
   */
  @Callback(Callbacks.COLLECT_REWARDS)
  public FeeDistributionContractState collectRewards(
      SysContractContext context,
      FeeDistributionContractState state,
      CallbackContext callbackContext,
      long epoch,
      SafeDataInputStream rpc) {
    FixedList<CallbackContext.ExecutionResult> results = callbackContext.results();
    for (int i = 0; i < results.size(); i++) {
      ShardId shard = new ShardId(rpc.readOptional(SafeDataInputStream::readString));
      CallbackContext.ExecutionResult collectedFees = results.get(i);
      if (collectedFees.isSucceeded()) {
        state = state.storeCollectedFees(collectedFees, shard).payBlockProducers(context);
      }
    }
    return state.cleanEpochInformation(epoch);
  }

  static long convertBlockProductionTimeToEpoch(long blockProductionTime) {
    return blockProductionTime >>> 21;
  }

  private static void ensure(boolean predicate, String errorString) {
    if (!predicate) {
      throw new RuntimeException(errorString);
    }
  }

  /**
   * Rpc friendly wrapper for Epoch Metrics.
   *
   * @param metrics the epoch metrics
   */
  record EpochMetricsRpc(List<Short> metrics) {

    EpochMetrics convert() {
      return new EpochMetrics(metrics.stream().map(s -> (int) s).toList());
    }
  }
}
