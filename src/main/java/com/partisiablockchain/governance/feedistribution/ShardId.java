package com.partisiablockchain.governance.feedistribution;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.contract.reflect.RpcType;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializableInline;

/** Wrapper class for the shard, since the shard id can be null. */
@Immutable
public record ShardId(@RpcType(nullable = true) String shardId)
    implements StateSerializableInline, Comparable<ShardId> {

  static ShardId createFromStateAccessor(StateAccessor accessor) {
    String shardId = accessor.get("shardId").stringValue();
    return new ShardId(shardId);
  }

  @Override
  public int compareTo(ShardId other) {
    if (equals(other)) {
      return 0;
    }
    if (shardId == null) {
      return -1;
    } else if (other.shardId == null) {
      return 1;
    } else {
      return shardId.compareTo(other.shardId);
    }
  }
}
