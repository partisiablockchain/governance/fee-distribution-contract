package com.partisiablockchain.util;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.math.Unsigned256;
import java.math.BigInteger;

/** Utility class for Unsigned256. */
public final class Unsigned256Util {

  @SuppressWarnings("unused")
  private Unsigned256Util() {}

  /**
   * Converts Unsigned256 to long.
   *
   * @param amount unsigned value
   * @return converted long value
   */
  public static long longValue(Unsigned256 amount) {
    return new BigInteger(amount.toString()).longValue();
  }

  /**
   * Performs min operation of two values.
   *
   * @param left one value
   * @param right another values
   * @return the smallest of the two values
   */
  @SuppressWarnings({"ComparatorResultComparison", "CompareToZero"})
  public static Unsigned256 min(Unsigned256 left, Unsigned256 right) {
    return left.compareTo(right) == -1 ? left : right;
  }

  /**
   * Performs Unsigned256 division and rounds up the remainder.
   *
   * @param dividend the dividend
   * @param divisor the divisor
   * @return the result
   */
  public static Unsigned256 ceilDiv(Unsigned256 dividend, Unsigned256 divisor) {
    if (dividend.equals(Unsigned256.ZERO)) {
      return Unsigned256.ZERO;
    }

    Unsigned256 x = dividend.add(divisor.subtract(Unsigned256.ONE));
    return x.divide(divisor);
  }
}
