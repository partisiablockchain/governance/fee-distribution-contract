package com.partisiablockchain.governance.feedistribution;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class FeeDistributionContractStateTest {

  private final List<BlockchainAddress> blockProducers =
      List.of(
          BlockchainAddress.fromString("000000000000000000000000000000000000000005"),
          BlockchainAddress.fromString("000000000000000000000000000000000000000006"),
          BlockchainAddress.fromString("000000000000000000000000000000000000000007"));

  private final BlockchainAddress bpOrchestrationContract =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.CONTRACT_GOV,
          Hash.create(h -> h.writeString("BpOrchestrationContract")));

  @Test
  public void determineCoinsToPay() {
    FeeDistributionContractTest.GlobalState globalState =
        new FeeDistributionContractTest.GlobalState();
    globalState.setCoinSymbols(List.of("DEMO_COIN"));
    final AccountStateHelper global = new AccountStateHelper(globalState);

    FixedList<ConvertedCoin> externalCoins =
        FixedList.create(
            List.of(
                ConvertedCoin.create()
                    .addConvertedCoins(Unsigned256.TEN, Unsigned256.create(100))));
    FeeDistributionContractState state =
        FeeDistributionContractState.initial(
                bpOrchestrationContract, FixedList.create(blockProducers), FixedList.create())
            .updatedExternalCoins(externalCoins);

    List<CoinToPay> coinToPay =
        FeeDistributionContractState.determineCoinsToPay(
                Unsigned256.create(99), 0, global, state.getExternalCoins())
            .getCoinsToPay();
    Assertions.assertThat(coinToPay.get(0).getCoinSymbol()).isEqualTo("DEMO_COIN");
    Assertions.assertThat(coinToPay.get(0).getCoins()).isEqualTo(Unsigned256.create(9));
  }

  @Test
  public void determineCoinsToPay_MultipleCoins() {
    FeeDistributionContractTest.GlobalState globalState =
        new FeeDistributionContractTest.GlobalState();
    globalState.setCoinSymbols(List.of("DEMO_COIN", "OTHER_COIN"));
    final AccountStateHelper global = new AccountStateHelper(globalState);

    List<ConvertedCoin> externalCoins =
        List.of(
            ConvertedCoin.create().addConvertedCoins(Unsigned256.TEN, Unsigned256.create(1000)),
            ConvertedCoin.create()
                .addConvertedCoins(Unsigned256.create(5), Unsigned256.create(1000)));

    FeeDistributionContractState state =
        FeeDistributionContractState.initial(
                bpOrchestrationContract, FixedList.create(blockProducers), FixedList.create())
            .updatedExternalCoins(FixedList.create(externalCoins));

    List<CoinToPay> coinToPay =
        FeeDistributionContractState.determineCoinsToPay(
                Unsigned256.create(2000), 0, global, state.getExternalCoins())
            .getCoinsToPay();

    CoinToPay coinZero = coinToPay.get(0);
    Assertions.assertThat(coinZero.getCoinSymbol()).isEqualTo("DEMO_COIN");
    Assertions.assertThat(coinZero.getCoins()).isEqualTo(Unsigned256.TEN);
    CoinToPay coinOne = coinToPay.get(1);
    Assertions.assertThat(coinOne.getCoinSymbol()).isEqualTo("OTHER_COIN");
    Assertions.assertThat(coinOne.getCoins()).isEqualTo(Unsigned256.create(5));
  }

  @Test
  public void determineCoinsToPay_MultipleCoins_MoreGlobalCoins() {
    FeeDistributionContractTest.GlobalState globalState =
        new FeeDistributionContractTest.GlobalState();
    globalState.setCoinSymbols(List.of("DEMO_COIN", "OTHER_COIN", "ANOTHER_COIN"));
    final AccountStateHelper global = new AccountStateHelper(globalState);

    FixedList<ConvertedCoin> current = FixedList.create();
    current =
        ConvertedCoin.COINS_LIST.withEntry(
            current,
            0,
            ConvertedCoin.create().addConvertedCoins(Unsigned256.TEN, Unsigned256.create(1000)));
    current =
        ConvertedCoin.COINS_LIST.withEntry(
            current, 2, ConvertedCoin.create().addConvertedCoins(Unsigned256.TEN, Unsigned256.TEN));

    FeeDistributionContractState state =
        FeeDistributionContractState.initial(
                bpOrchestrationContract, FixedList.create(blockProducers), FixedList.create())
            .updatedExternalCoins(current);

    List<CoinToPay> coinToPay =
        FeeDistributionContractState.determineCoinsToPay(
                Unsigned256.create(1010), 0, global, state.getExternalCoins())
            .getCoinsToPay();

    Assertions.assertThat(coinToPay.get(0).getCoinSymbol()).isEqualTo("DEMO_COIN");
    Assertions.assertThat(coinToPay.get(0).getCoins()).isEqualTo(Unsigned256.TEN);

    Assertions.assertThat(coinToPay.get(1).getCoinSymbol()).isEqualTo("ANOTHER_COIN");
    Assertions.assertThat(coinToPay.get(1).getCoins()).isEqualTo(Unsigned256.TEN);

    coinToPay =
        FeeDistributionContractState.determineCoinsToPay(
                Unsigned256.create(1010), 2, global, state.getExternalCoins())
            .getCoinsToPay();

    Assertions.assertThat(coinToPay.get(0).getCoinSymbol()).isEqualTo("ANOTHER_COIN");
    Assertions.assertThat(coinToPay.get(0).getCoins()).isEqualTo(Unsigned256.TEN);

    Assertions.assertThat(coinToPay.get(1).getCoinSymbol()).isEqualTo("DEMO_COIN");
    Assertions.assertThat(coinToPay.get(1).getCoins()).isEqualTo(Unsigned256.TEN);
  }

  @Test
  public void determineCoinsToPay_NotEnoughCoins() {
    FeeDistributionContractTest.GlobalState globalState =
        new FeeDistributionContractTest.GlobalState();
    globalState.setCoinSymbols(List.of("DEMO_COIN"));
    final AccountStateHelper global = new AccountStateHelper(globalState);

    List<ConvertedCoin> externalCoins =
        List.of(
            ConvertedCoin.create()
                .addConvertedCoins(Unsigned256.create(13), Unsigned256.create(500)));

    FeeDistributionContractState state =
        FeeDistributionContractState.initial(
                bpOrchestrationContract, FixedList.create(blockProducers), FixedList.create())
            .updatedExternalCoins(FixedList.create(externalCoins));

    DetermineCoinsResult result =
        FeeDistributionContractState.determineCoinsToPay(
            Unsigned256.create(200), 0, global, state.getExternalCoins());
    List<CoinToPay> coinToPay = result.getCoinsToPay();

    CoinToPay coinZero = coinToPay.get(0);
    Assertions.assertThat(coinZero.getCoinSymbol()).isEqualTo("DEMO_COIN");
    Assertions.assertThat(coinZero.getCoins()).isEqualTo(Unsigned256.create(5));
    Assertions.assertThat(result.getRemaining()).isEqualTo(Unsigned256.create(7));
  }

  @Test
  public void storeCollectedFees_NoNotifyingBlockProducers() {
    List<ShardId> shards = List.of(new ShardId("Shard0"), new ShardId("Shard1"));
    Unsigned256 blockchainUsage = Unsigned256.create(3);
    long epoch = 1;

    FeeDistributionContractState state =
        FeeDistributionContractState.initial(
            bpOrchestrationContract, FixedList.create(blockProducers), FixedList.create(shards));

    AvlTree<Long, Unsigned256> shardOneUsage = AvlTree.create(Map.of(epoch, blockchainUsage));

    CallbackContext.ExecutionResult executionResult1 =
        FeeDistributionContractTest.createExecutionResult(
            FixedList.create(), shardOneUsage, AvlTree.create());
    FeeDistributionContractState collectedFees =
        state.storeCollectedFees(executionResult1, shards.get(0));
    for (BlockchainAddress blockProducer : blockProducers) {
      Assertions.assertThat(collectedFees.getFeesToDistribute().getValue(blockProducer))
          .isEqualTo(blockchainUsage.divide(Unsigned256.create(blockProducers.size())));
    }
  }

  @Test
  public void gasRewardPoolCanAlternateBetweenPositiveAndNegative() {
    List<ShardId> shards = List.of(new ShardId("Shard0"));
    FeeDistributionContractState state =
        FeeDistributionContractState.initial(
            bpOrchestrationContract, FixedList.create(blockProducers), FixedList.create(shards));

    // Add 1 -> val: 1, sign: pos
    Signed256 updatedGasRewardPool = state.getGasRewardPool().add(Unsigned256.ONE);
    FeeDistributionContractState updatedState = state.updatedRewardPool(updatedGasRewardPool);
    Assertions.assertThat(updatedState.getGasRewardPool().getAbsoluteValue())
        .isEqualTo(Unsigned256.create(1L));
    Assertions.assertThat(updatedState.getGasRewardPool().isPositive()).isEqualTo(true);

    // subtract 10 -> val: 9, sign: neg
    updatedGasRewardPool = updatedState.getGasRewardPool().subtract(Unsigned256.TEN);
    updatedState = updatedState.updatedRewardPool(updatedGasRewardPool);
    Assertions.assertThat(updatedState.getGasRewardPool().getAbsoluteValue())
        .isEqualByComparingTo(Unsigned256.create(9L));
    Assertions.assertThat(updatedState.getGasRewardPool().isPositive()).isEqualTo(false);

    // Alt 1
    // Add 9 -> val: 0, sign: pos
    Signed256 alternativeGasRewardPool = updatedState.getGasRewardPool().add(Unsigned256.create(9));
    FeeDistributionContractState alternativeState =
        updatedState.updatedRewardPool(alternativeGasRewardPool);
    Assertions.assertThat(alternativeState.getGasRewardPool().getAbsoluteValue())
        .isEqualTo(Unsigned256.ZERO);
    Assertions.assertThat(alternativeState.getGasRewardPool().isPositive()).isEqualTo(true);

    // Alt 2
    // Add 8 -> val: 1, sign: neg
    alternativeGasRewardPool = updatedState.getGasRewardPool().add(Unsigned256.create(8));
    alternativeState = updatedState.updatedRewardPool(alternativeGasRewardPool);
    Assertions.assertThat(alternativeState.getGasRewardPool().getAbsoluteValue())
        .isEqualTo(Unsigned256.ONE);
    Assertions.assertThat(alternativeState.getGasRewardPool().isPositive()).isEqualTo(false);

    // Alt 3
    // Subtract 8 -> val: 1, sign: neg
    alternativeGasRewardPool = updatedState.getGasRewardPool().subtract(Unsigned256.create(8));
    alternativeState = updatedState.updatedRewardPool(alternativeGasRewardPool);
    Assertions.assertThat(alternativeState.getGasRewardPool().getAbsoluteValue())
        .isEqualTo(Unsigned256.create(17L));
    Assertions.assertThat(alternativeState.getGasRewardPool().isPositive()).isEqualTo(false);

    // Add 10 -> val: 1, sign: pos
    updatedGasRewardPool = updatedState.getGasRewardPool().add(Unsigned256.TEN);
    updatedState = updatedState.updatedRewardPool(updatedGasRewardPool);
    Assertions.assertThat(updatedState.getGasRewardPool().getAbsoluteValue())
        .isEqualTo(Unsigned256.create(1L));
    Assertions.assertThat(updatedState.getGasRewardPool().isPositive()).isEqualTo(true);
  }

  @Test
  void collectRewardsCallback() {
    long epoch = 10;
    ShardId shardOne = new ShardId("ShardOne");
    List<ShardId> shardIdList = List.of(shardOne);
    byte[] rpc =
        SafeDataOutputStream.serialize(
            FeeDistributionContract.Callbacks.collectRewards(epoch, FixedList.create(shardIdList)));

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    Assertions.assertThat(stream.readUnsignedByte())
        .isEqualTo(FeeDistributionContract.Callbacks.COLLECT_REWARDS);
    Assertions.assertThat(stream.readLong()).isEqualTo(epoch);
    Assertions.assertThat(stream.readOptional(SafeDataInputStream::readString))
        .isEqualTo(shardOne.shardId());
  }
}
