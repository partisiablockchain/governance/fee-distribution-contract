package com.partisiablockchain.governance.feedistribution;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.math.Unsigned256;
import com.secata.stream.SafeDataInputStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

final class AccountPluginRpcTest {
  @Test
  void collectServiceAndInfrastructureFeesForDistribution() {
    long epoch = 10;

    byte[] rpc = AccountPluginRpc.collectServiceAndInfrastructureFeesForDistribution(epoch);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    Assertions.assertThat(stream.readUnsignedByte())
        .isEqualTo(
            AccountPluginRpc
                .COLLECT_SERVICE_AND_INFRASTRUCTURE_FEES_FOR_DISTRIBUTION_INVOCATION_BYTE);
    Assertions.assertThat(stream.readLong()).isEqualTo(epoch);
  }

  @Test
  void addCoinBalance() {
    String coinSymbol = "DEMO_COIN";
    CoinToPay coinToPay = new CoinToPay(0, coinSymbol, Unsigned256.TEN, Unsigned256.ONE);

    byte[] rpc = AccountPluginRpc.addCoinBalance(coinToPay);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    Assertions.assertThat(stream.readUnsignedByte())
        .isEqualTo(AccountPluginRpc.ADD_COIN_BALANCE_INVOCATION_BYTE);
    Assertions.assertThat(stream.readString()).isEqualTo(coinSymbol);
    Assertions.assertThat(Unsigned256.read(stream)).isEqualTo(coinToPay.getCoins());
  }
}
