package com.partisiablockchain.governance.feedistribution;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.governance.feedistribution.FeeDistributionContractState.PAYOUT_THRESHOLD;

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.ContractEventGroup;
import com.partisiablockchain.contract.SysContractContextTest;
import com.partisiablockchain.contract.SysContractSerialization;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateObjectMapper;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.partisiablockchain.util.Unsigned256Util;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.stream.SafeListStream;
import com.secata.tools.immutable.FixedList;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Stream;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.ThrowableAssert;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test for {@link FeeDistributionContract}. */
public final class FeeDistributionContractTest {

  private final BlockchainAddress accountOne =
      BlockchainAddress.fromString("000000000000000000000000000000000000000001");

  private final List<BlockchainAddress> blockProducers =
      List.of(
          BlockchainAddress.fromString("000000000000000000000000000000000000000005"),
          BlockchainAddress.fromString("000000000000000000000000000000000000000006"),
          BlockchainAddress.fromString("000000000000000000000000000000000000000007"),
          BlockchainAddress.fromString("000000000000000000000000000000000000000008"),
          BlockchainAddress.fromString("000000000000000000000000000000000000000009"));

  private final List<BlockchainAddress> newCommittee =
      List.of(
          BlockchainAddress.fromString("000000000000000000000000000000000000000005"),
          BlockchainAddress.fromString("000000000000000000000000000000000000000006"),
          BlockchainAddress.fromString("000000000000000000000000000000000000000007"),
          BlockchainAddress.fromString("000000000000000000000000000000000000000008"),
          BlockchainAddress.fromString("000000000000000000000000000000000000000009"),
          BlockchainAddress.fromString("000000000000000000000000000000000000000010"));

  private final BlockchainAddress bpOrchestrationContract =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.CONTRACT_GOV,
          Hash.create(h -> h.writeString("BpOrchestrationContract")));

  private final List<ShardId> shardIds =
      List.of(new ShardId(null), new ShardId("Shard1"), new ShardId("Shard2"));

  private final SysContractSerialization<FeeDistributionContractState> serialization =
      new SysContractSerialization<>(
          FeeDistributionContractInvoker.class, FeeDistributionContractState.class);

  private final GlobalState global = new GlobalState();
  private SysContractContextTest context;
  private static final byte INVALID_INVOCATION_BYTE = 100;

  private static final SafeListStream<ShardId> SHARD_ID_LIST_SERIALIZER =
      SafeListStream.create(
          stream -> new ShardId(stream.readOptional(SafeDataInputStream::readString)),
          (s, stream) ->
              stream.writeOptional(
                  SafeListStream.primitive(SafeDataOutputStream::writeString), s.shardId()));

  /** Sets up the test. */
  @BeforeEach
  public void setUp() {
    context = new SysContractContextTest();
    context.setGlobalAccountPluginState(global);
    global.setCoinSymbols(List.of("DEMO_COIN", "OTHER_COIN"));
  }

  @Test
  public void create() {
    FeeDistributionContractState state =
        serialization.create(
            context,
            rpc -> {
              bpOrchestrationContract.write(rpc);
              SHARD_ID_LIST_SERIALIZER.writeDynamic(rpc, shardIds);
            });
    Assertions.assertThat(state).isNotNull();
    FixedList<BlockchainAddress> blockProducers = state.getBlockProducers();
    Assertions.assertThat(blockProducers.size()).isEqualTo(0);
    Assertions.assertThat(state.getBpOrchestrationContract()).isEqualTo(bpOrchestrationContract);
    Assertions.assertThat(state.getShardIds())
        .usingRecursiveComparison()
        .isEqualTo(FixedList.create(shardIds));
    List<ShardId> shardIdsRead =
        SHARD_ID_LIST_SERIALIZER.readDynamic(
            SafeDataInputStream.createFromBytes(
                SafeDataOutputStream.serialize(
                    rpc -> SHARD_ID_LIST_SERIALIZER.writeDynamic(rpc, shardIds))));
    Assertions.assertThat(shardIdsRead).isEqualTo(shardIds);
  }

  @Test
  public void invokeUpdateCommittee() {
    FeeDistributionContractState initial =
        FeeDistributionContractState.initial(
            bpOrchestrationContract, FixedList.create(blockProducers), FixedList.create());

    context = new SysContractContextTest(System.currentTimeMillis(), 77, bpOrchestrationContract);
    FeeDistributionContractState newState =
        serialization.invoke(
            context,
            initial,
            rpc -> {
              rpc.writeByte(FeeDistributionContract.Invocations.UPDATE_COMMITTEE);
              BlockchainAddress.LIST_SERIALIZER.writeDynamic(rpc, newCommittee);
            });
    Assertions.assertThat(newState.getBlockProducers()).isNotEqualTo(initial.getBlockProducers());
    Assertions.assertThat(newState.getBlockProducers().size())
        .isEqualTo(initial.getBlockProducers().size() + 1);
  }

  @Test
  public void invokeInvalidInvocation() {
    FeeDistributionContractState initial =
        FeeDistributionContractState.initial(
            bpOrchestrationContract, FixedList.create(blockProducers), FixedList.create());

    context = new SysContractContextTest(System.currentTimeMillis(), 77, accountOne);
    ThrowableAssert.ThrowingCallable notBpOrchestration =
        () ->
            serialization.invoke(
                context,
                initial,
                rpc -> {
                  rpc.writeByte(INVALID_INVOCATION_BYTE);
                  BlockchainAddress.LIST_SERIALIZER.writeDynamic(rpc, List.of());
                });
    Assertions.assertThatThrownBy(notBpOrchestration)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Invalid shortname: 100");
  }

  @Test
  public void invokeUpdateCommittee_Invalid() {
    FeeDistributionContractState initial =
        FeeDistributionContractState.initial(
            bpOrchestrationContract, FixedList.create(blockProducers), FixedList.create());

    context = new SysContractContextTest(System.currentTimeMillis(), 77, accountOne);
    ThrowableAssert.ThrowingCallable notBpOrchestration =
        () ->
            serialization.invoke(
                context,
                initial,
                rpc -> {
                  rpc.writeByte(FeeDistributionContract.Invocations.UPDATE_COMMITTEE);
                  BlockchainAddress.LIST_SERIALIZER.writeDynamic(rpc, List.of());
                });
    Assertions.assertThatThrownBy(notBpOrchestration)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only the BP orchestration contract can update the committee");

    context = new SysContractContextTest(System.currentTimeMillis(), 77, bpOrchestrationContract);
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    initial,
                    rpc -> {
                      rpc.writeByte(FeeDistributionContract.Invocations.UPDATE_COMMITTEE);
                      BlockchainAddress.LIST_SERIALIZER.writeDynamic(rpc, List.of());
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("A fee distribution contract cannot be updated without block producers");
  }

  @Test
  public void invokeNotify_RegisterEndedEpochs() {
    ShardId shardOne = shardIds.get(0);
    long epoch = 1;

    FeeDistributionContractState registerEndedState =
        FeeDistributionContractState.initial(
            bpOrchestrationContract, FixedList.create(blockProducers), FixedList.create(shardIds));

    for (int bpIndex = 0; bpIndex < blockProducers.size(); bpIndex++) {
      context =
          new SysContractContextTest(
              getBlockProductionTimeForEpochTwo(), 77, blockProducers.get(bpIndex));
      registerEndedState =
          serialization.invoke(context, registerEndedState, createNotifyRpc(shardOne, epoch));

      Assertions.assertThat(
              registerEndedState
                  .getEpochShardInformation(epoch)
                  .getBlockProducersForShardBasedOnMetrics(
                      shardOne, FixedList.create(blockProducers))
                  .size())
          .isEqualTo(bpIndex + 1);
    }

    Assertions.assertThat(
            registerEndedState
                .getEpochShardInformation(epoch)
                .getBlockProducersForShardBasedOnMetrics(shardOne, FixedList.create(blockProducers))
                .size())
        .isEqualTo(blockProducers.size());
  }

  private Consumer<SafeDataOutputStream> createNotifyRpc(ShardId shard, long epoch) {
    return rpc -> {
      rpc.writeByte(FeeDistributionContract.Invocations.NOTIFY);
      rpc.writeOptional(
          SafeListStream.primitive(SafeDataOutputStream::writeString), shard.shardId());
      rpc.writeLong(epoch);
      rpc.writeInt(0);
    };
  }

  @Test
  void invokeWithMetrics() {
    FeeDistributionContractState state =
        FeeDistributionContractState.initial(
            bpOrchestrationContract, FixedList.create(blockProducers), FixedList.create(shardIds));
    context =
        new SysContractContextTest(getBlockProductionTimeForEpochTwo(), 1, blockProducers.get(0));
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(FeeDistributionContract.Invocations.NOTIFY);
              rpc.writeOptional(
                  SafeListStream.primitive(SafeDataOutputStream::writeString),
                  shardIds.get(0).shardId());
              rpc.writeLong(1);
              rpc.writeInt(2);
              rpc.writeShort(5);
              rpc.writeShort(3);
            });
    Assertions.assertThat(state).isNotNull();
    Assertions.assertThat(
            state
                .getEpochShardInformation(1L)
                .getShards()
                .getValue(shardIds.get(0))
                .getMetrics()
                .getValue(blockProducers.get(0))
                .getSignatureFrequencies())
        .containsExactly(5, 3);
  }

  @Test
  public void invokeNotify_NonBlockProducer() {
    context = new SysContractContextTest(System.currentTimeMillis(), 77, accountOne);
    FeeDistributionContractState initial =
        FeeDistributionContractState.initial(
            bpOrchestrationContract, FixedList.create(blockProducers), FixedList.create());
    long epoch = 0;
    Assertions.assertThatThrownBy(
            () -> serialization.invoke(context, initial, createNotifyRpc(new ShardId(null), epoch)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only block producers can notify an epoch has ended");
  }

  @Test
  public void invokeNotify_TriggerConfirmedEnded() {
    ShardId shardOne = shardIds.get(0);
    long epochConfirmedEnded = 0;
    final long epochEnded = 1;

    final FeeDistributionContractState stateWithGasToDistribute =
        FeeDistributionContractState.initial(
            bpOrchestrationContract, FixedList.create(blockProducers), FixedList.create(shardIds));

    final FeeDistributionContractState notConfirmedEnded =
        stateWithGasToDistribute
            .registerEpochEnded(
                blockProducers.get(0), shardOne, epochConfirmedEnded, new EpochMetrics(List.of()))
            .registerEpochEnded(
                blockProducers.get(1), shardOne, epochConfirmedEnded, new EpochMetrics(List.of()))
            .registerEpochEnded(
                blockProducers.get(2), shardOne, epochConfirmedEnded, new EpochMetrics(List.of()));
    Assertions.assertThat(context.getRemoteCalls()).isNull();
    Assertions.assertThat(context.getInteractions()).isEmpty();

    context =
        new SysContractContextTest(getBlockProductionTimeForEpochTwo(), 77, blockProducers.get(0));
    final FeeDistributionContractState rewardsCollectedState =
        serialization.invoke(context, notConfirmedEnded, createNotifyRpc(shardOne, epochEnded));

    ContractEventGroup remoteCalls = context.getRemoteCalls();
    byte[] actualCallbackRpc = remoteCalls.callbackRpc;
    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(
            FeeDistributionContract.Callbacks.collectRewards(
                epochConfirmedEnded, FixedList.create(shardIds)));
    Assertions.assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);

    byte[] actualRpc = context.getUpdateContextFreeAccountPluginState();
    byte[] expectedRpc =
        AccountPluginRpc.collectServiceAndInfrastructureFeesForDistribution(epochConfirmedEnded);
    Assertions.assertThat(actualRpc).isEqualTo(expectedRpc);

    context =
        new SysContractContextTest(getBlockProductionTimeForEpochTwo(), 77, blockProducers.get(1));
    serialization.invoke(context, rewardsCollectedState, createNotifyRpc(shardOne, epochEnded));
    Assertions.assertThat(context.getRemoteCalls()).isNull();
    Assertions.assertThat(context.getInteractions()).isEmpty();
  }

  @Test
  public void invokeNotify_BlockProducerNotifyingSameEpochTwice() {
    context = new SysContractContextTest(getBlockProductionTimeForEpochTwo(), 77, accountOne);
    ShardId shard = shardIds.get(0);
    FeeDistributionContractState initial =
        FeeDistributionContractState.initial(
            bpOrchestrationContract, FixedList.create(blockProducers), FixedList.create(shardIds));
    BlockchainAddress notifyingBlockProducer = blockProducers.get(1);
    context =
        new SysContractContextTest(getBlockProductionTimeForEpochTwo(), 77, notifyingBlockProducer);
    long epoch = 1;
    EpochMetrics dummyMetrics = new EpochMetrics(List.of());
    FeeDistributionContractState existingNotifyFromProducer =
        initial.registerEpochEnded(notifyingBlockProducer, shard, epoch, dummyMetrics);
    FeeDistributionContractState tryToNotifyTwice =
        serialization.invoke(context, existingNotifyFromProducer, createNotifyRpc(shard, epoch));
    Assertions.assertThat(
            tryToNotifyTwice
                .getEpochShardInformation(epoch)
                .getBlockProducersForShardBasedOnMetrics(shard, FixedList.create(blockProducers)))
        .containsExactly(notifyingBlockProducer);
  }

  @Test
  public void invokeNotify_Invalid() {
    context =
        new SysContractContextTest(getBlockProductionTimeForEpochTwo(), 77, blockProducers.get(0));
    FeeDistributionContractState initial =
        FeeDistributionContractState.initial(
            bpOrchestrationContract, FixedList.create(blockProducers), FixedList.create(shardIds));
    ShardId shard = shardIds.get(0);
    ThrowableAssert.ThrowingCallable sameEpoch =
        () -> serialization.invoke(context, initial, createNotifyRpc(shard, 2));
    Assertions.assertThatThrownBy(sameEpoch)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("It is only allowed to notify an epoch has ended in the subsequent epoch");
    ThrowableAssert.ThrowingCallable notSubsequent =
        () -> serialization.invoke(context, initial, createNotifyRpc(shard, 0));
    Assertions.assertThatThrownBy(notSubsequent)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("It is only allowed to notify an epoch has ended in the subsequent epoch");
    ThrowableAssert.ThrowingCallable unknownShard =
        () ->
            serialization.invoke(context, initial, createNotifyRpc(new ShardId("UnknownShard"), 0));
    Assertions.assertThatThrownBy(unknownShard)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("The provided shard does not correspond to a known shard");
  }

  @Test
  public void invokeOnUpdate() {
    FixedList<BlockchainAddress> blockProducerList = FixedList.create(blockProducers);
    FixedList<ShardId> shardIdList = FixedList.create(shardIds);
    Unsigned256 coinAmount = Unsigned256.create(10);

    ConvertedCoin externalCoin =
        ConvertedCoin.create()
            .addConvertedCoins(
                coinAmount,
                Unsigned256.create(PAYOUT_THRESHOLD / 10 * Unsigned256Util.longValue(coinAmount)));

    FixedList<ConvertedCoin> initCoins = FixedList.create(List.of(externalCoin));
    AvlTree<BlockchainAddress, Unsigned256> initFeeTree = AvlTree.create();
    initFeeTree = initFeeTree.set(accountOne, Unsigned256.create(10));

    FeeDistributionContractState initial =
        FeeDistributionContractState.initial(
                bpOrchestrationContract, blockProducerList, shardIdList)
            .updatedFeesToDistribute(initFeeTree)
            .updatedExternalCoins(initCoins)
            .registerEpochEnded(accountOne, shardIds.get(1), 15, new EpochMetrics(List.of()));

    FeeDistributionContract contract = new FeeDistributionContract();
    StateAccessor accessor = StateAccessor.create(initial);

    FeeDistributionContractState updated = contract.upgrade(accessor);

    Assertions.assertThat(updated.getBpOrchestrationContract())
        .isEqualTo(initial.getBpOrchestrationContract());

    Assertions.assertThat(updated.getBlockProducers()).isEqualTo(initial.getBlockProducers());

    for (int i = 0; i < initCoins.size(); i++) {
      Assertions.assertThat(initCoins.get(i).getConvertedCoins())
          .isEqualTo(updated.getExternalCoins().get(i).getConvertedCoins());
      Assertions.assertThat(initCoins.get(i).getConvertedGasSum())
          .isEqualTo(updated.getExternalCoins().get(i).getConvertedGasSum());
    }

    AvlTree<BlockchainAddress, Unsigned256> initialFeesTree = initial.getFeesToDistribute();
    AvlTree<BlockchainAddress, Unsigned256> updateFeesTree = updated.getFeesToDistribute();

    Assertions.assertThat(initialFeesTree).isEqualTo(updateFeesTree);

    Assertions.assertThat(initialFeesTree.values()).isEqualTo(updateFeesTree.values());

    for (BlockchainAddress key : initialFeesTree.keySet()) {
      Assertions.assertThat(initialFeesTree.getValue(key)).isEqualTo(updateFeesTree.getValue(key));
    }

    EpochInformation initialEpochsInformation = initial.getEpochShardInformation(15L);
    EpochInformation updateEpochsInformation = updated.getEpochShardInformation(15L);

    Assertions.assertThat(
            initialEpochsInformation.getBlockProducersForShardBasedOnMetrics(
                shardIds.get(1), FixedList.create(blockProducers)))
        .isEqualTo(
            updateEpochsInformation.getBlockProducersForShardBasedOnMetrics(
                shardIds.get(1), FixedList.create(blockProducers)));

    FixedList<ShardId> updatedShardIds = updated.getShardIds();
    for (int i = 0; i < shardIdList.size(); i++) {
      Assertions.assertThat(updatedShardIds.get(i)).isEqualTo(shardIdList.get(i));
    }
  }

  private long getBlockProductionTimeForEpochTwo() {
    return (long) Math.pow(2, 22);
  }

  @Test
  public void callback_PayBlockProducerInTwoDifferentCoins() {
    ShardId shardOne = shardIds.get(0);
    long epoch = 1;

    AvlTree<Long, EpochInformation> epochs = createEpochs(shardOne, epoch);

    Assertions.assertThat(epochs.size()).isEqualTo(3);

    BlockchainAddress blockProducer = blockProducers.get(0);
    FeeDistributionContractState confirmEpochEndedState =
        new FeeDistributionContractState(
                bpOrchestrationContract,
                FixedList.create(blockProducers),
                FixedList.create(),
                AvlTree.create(),
                epochs,
                -1,
                FixedList.create(shardIds),
                Signed256.create())
            .registerEpochEnded(blockProducer, shardOne, epoch, new EpochMetrics(List.of()))
            .confirmEpochEnded(epoch, context);

    Unsigned256 firstCoinAmount = Unsigned256.create(9);
    Unsigned256 firstGasEquivalent =
        Unsigned256.create(PAYOUT_THRESHOLD / 10 * Unsigned256Util.longValue(firstCoinAmount));
    ConvertedCoin firstCoin =
        ConvertedCoin.create().addConvertedCoins(firstCoinAmount, firstGasEquivalent);
    Unsigned256 secondCoinAmount = Unsigned256.create(2);
    Unsigned256 secondGasEquivalent =
        Unsigned256.create(PAYOUT_THRESHOLD / 10 * Unsigned256Util.longValue(secondCoinAmount));
    ConvertedCoin secondCoin =
        ConvertedCoin.create().addConvertedCoins(secondCoinAmount, secondGasEquivalent);
    FixedList<ConvertedCoin> shardOneConvertedCoins =
        FixedList.create(List.of(firstCoin, secondCoin));

    // blockchain usage and service fees combined is equal to payout threshold,
    Unsigned256 deductedTax = calculateTaxForThreshold(Unsigned256.create(PAYOUT_THRESHOLD));
    AvlTree<Long, Unsigned256> shardOneBlockchainUsage =
        AvlTree.create(
            Map.of(
                epoch,
                Unsigned256.create(PAYOUT_THRESHOLD).subtract(Unsigned256.TEN).add(deductedTax)));
    AvlTree<BlockchainAddress, Unsigned256> shardOneServiceFees =
        AvlTree.create(Map.of(blockProducer, Unsigned256.TEN));
    List<CallbackContext.ExecutionResult> resultsShardOne =
        List.of(
            createExecutionResult(
                shardOneConvertedCoins, shardOneBlockchainUsage, shardOneServiceFees));

    CallbackContext callbackContext = CallbackContext.create(FixedList.create(resultsShardOne));
    FeeDistributionContractState coinsPaidOutState =
        serialization.callback(
            context,
            callbackContext,
            confirmEpochEndedState,
            FeeDistributionContract.Callbacks.collectRewards(
                epoch, FixedList.create(List.of(shardOne))));

    Unsigned256 infrastructureFeeSum = Unsigned256.TEN;
    Assertions.assertThat(coinsPaidOutState.getGasRewardPool().getAbsoluteValue())
        .isEqualTo(deductedTax.subtract(infrastructureFeeSum));
    Assertions.assertThat(coinsPaidOutState.getFeesToDistribute().size()).isEqualTo(0);

    int noOfShardsForPreviousEpoch =
        coinsPaidOutState.getEpochShardInformation(epoch - 1).getShards().size();
    Assertions.assertThat(noOfShardsForPreviousEpoch).isEqualTo(0);

    int noOfShardsForCurrentEpoch =
        coinsPaidOutState.getEpochShardInformation(epoch).getShards().size();
    Assertions.assertThat(noOfShardsForCurrentEpoch).isEqualTo(0);

    int noOfShardsForFutureEpoch =
        coinsPaidOutState.getEpochShardInformation(epoch + 1).getShards().size();
    Assertions.assertThat(noOfShardsForFutureEpoch).isEqualTo(1);
    CoinToPay firstCoinToPay = new CoinToPay(0, "DEMO_COIN", firstCoinAmount, firstGasEquivalent);
    byte[] actualRpc = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] expectedRpc = AccountPluginRpc.addCoinBalance(firstCoinToPay);
    Assertions.assertThat(actualRpc).isEqualTo(expectedRpc);

    CoinToPay secondCoinToPay =
        new CoinToPay(
            1, "OTHER_COIN", secondCoinAmount.subtract(Unsigned256.ONE), secondGasEquivalent);
    actualRpc = context.getUpdateLocalAccountPluginStates().get(1).getRpc();
    expectedRpc = AccountPluginRpc.addCoinBalance(secondCoinToPay);
    Assertions.assertThat(actualRpc).isEqualTo(expectedRpc);

    SafeDataInputStream secondCoinRpc =
        SafeDataInputStream.createFromBytes(
            context.getUpdateLocalAccountPluginStates().get(1).getRpc());
    Assertions.assertThat(secondCoinRpc.readSignedByte()).isEqualTo((byte) 0); // ADD_COIN_BALANCE
    Assertions.assertThat(secondCoinRpc.readString()).isEqualTo("OTHER_COIN");
    Assertions.assertThat(Unsigned256.read(secondCoinRpc))
        .isEqualTo(secondCoinAmount.subtract(Unsigned256.ONE));

    // blockchain usage and service fees combined exceeds payout threshold
    AvlTree<BlockchainAddress, Unsigned256> shardOneServiceFeesOverThreshold =
        AvlTree.create(Map.of(blockProducer, Unsigned256.create(11)));
    List<CallbackContext.ExecutionResult> resultsShardOneOverThreshold =
        List.of(
            createExecutionResult(
                shardOneConvertedCoins, shardOneBlockchainUsage, shardOneServiceFeesOverThreshold));

    CallbackContext callbackContextOverThreshold =
        CallbackContext.create(FixedList.create(resultsShardOneOverThreshold));
    FeeDistributionContractState coinsPaidOutStateOverThreshold =
        serialization.callback(
            context,
            callbackContextOverThreshold,
            confirmEpochEndedState,
            FeeDistributionContract.Callbacks.collectRewards(
                epoch, FixedList.create(List.of(shardOne))));
    Assertions.assertThat(coinsPaidOutStateOverThreshold.getFeesToDistribute().size()).isEqualTo(1);
  }

  /**
   * Reproduction of an exception encountered in testnet. If multiple producers are to be paid the
   * same coins are used multiple times resulting in an underflow.
   *
   * @throws IOException if unable to read reference state
   */
  @Test
  public void shouldBeAbleToPayoutToMultipleProducersEvenIfFirstCoinRunsOut() throws IOException {
    FeeDistributionContractState confirmEpochEndedState =
        StateObjectMapper.createObjectMapper()
            .readValue(
                FeeDistributionContractState.class.getResource(
                    "shouldBeAbleToPayoutToMultipleProducersEvenIfFirstCoinRunsOut.json"),
                FeeDistributionContractState.class);

    String s0 =
        "0000000000000000000000000000000100000000000c17af00000000000000000000000000000000000000000"
            + "000000000000000002785c6000000000000000000000000000000000000000000000000000000000000"
            + "00000000000000000000";
    String s1 =
        "0000000000000000000000000000000100000000000c17af0000000000000000000000000000000000000000"
            + "0000000000000000000124f80000000000000000000000000000000000000000000000000000000000"
            + "0000000000000000000000";
    String s2 =
        "0000000000000000000000000000000100000000000c17af0000000000000000000000000000000000000000"
            + "0000000000000000005d14d00000000000000000000000000000000000000000000000000000000000"
            + "0000000000000000000000";
    String s3 =
        "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
            + "0000000000000000000000000";

    CallbackContext callbackContext =
        CallbackContext.create(
            FixedList.create(
                List.of(
                    CallbackContext.createResult(null, true, extractReturn(s0)),
                    CallbackContext.createResult(null, true, extractReturn(s1)),
                    CallbackContext.createResult(null, true, extractReturn(s2)),
                    CallbackContext.createResult(null, true, extractReturn(s3)))));
    serialization.callback(
        context,
        callbackContext,
        confirmEpochEndedState,
        rpc ->
            rpc.write(
                Hex.decode(
                    "0100000000000c17af01000000065368617264300100000"
                        + "006536861726431010000000653686172643200")));
    Assertions.assertThat(context.getUpdateLocalAccountPluginStates()).hasSize(5);
  }

  private SafeDataInputStream extractReturn(String s0) {
    return SafeDataInputStream.createFromBytes(Hex.decode(s0));
  }

  private AvlTree<Long, EpochInformation> createEpochs(ShardId shardId, long epoch) {
    AvlTree<ShardId, ShardInformation> noShards = AvlTree.create();
    AvlTree<ShardId, ShardInformation> oneShard = noShards.set(shardId, ShardInformation.initial());

    EpochInformation epochInformation = new EpochInformation(oneShard);
    AvlTree<Long, EpochInformation> epochs = AvlTree.create();
    return epochs
        .set(epoch - 1, epochInformation)
        .set(epoch, epochInformation)
        .set(epoch + 1, epochInformation);
  }

  @Test
  public void callback_Invalid() {
    SafeDataInputStream empty = SafeDataInputStream.createFromBytes(new byte[0]);
    CallbackContext.ExecutionResult result =
        CallbackContext.createResult(context.getCurrentTransactionHash(), false, empty);
    FixedList<CallbackContext.ExecutionResult> results = FixedList.create(Stream.of(result));
    CallbackContext callbackContext = CallbackContext.create(results);

    FeeDistributionContractState initial =
        FeeDistributionContractState.initial(
            bpOrchestrationContract, FixedList.create(blockProducers), FixedList.create(shardIds));
    FeeDistributionContractState failedCallbackState =
        serialization.callback(
            context,
            callbackContext,
            initial,
            FeeDistributionContract.Callbacks.collectRewards(
                1234, FixedList.create(List.of(new ShardId("TestShard")))));
    Assertions.assertThat(failedCallbackState).usingRecursiveComparison().isEqualTo(initial);
  }

  @Test
  public void callback_StoreCollectedFees() {
    long epoch = 1;

    BlockchainAddress blockProducer = blockProducers.get(0);

    Unsigned256 bpShardOneUsage = Unsigned256.create(4);
    Unsigned256 bpShardOneServiceFee = Unsigned256.create(50L);
    Unsigned256 bpShardTwoUsage = Unsigned256.create(6);
    Unsigned256 bpShardTwoServiceFee = Unsigned256.create(150L);

    final AvlTree<Long, Unsigned256> shardOneUsage = AvlTree.create(Map.of(epoch, bpShardOneUsage));
    final AvlTree<BlockchainAddress, Unsigned256> shardOneServiceFees =
        AvlTree.create(Map.of(blockProducer, bpShardOneServiceFee));

    final AvlTree<Long, Unsigned256> shardTwoUsage = AvlTree.create(Map.of(epoch, bpShardTwoUsage));
    final AvlTree<BlockchainAddress, Unsigned256> shardTwoServiceFees =
        AvlTree.create(Map.of(blockProducer, bpShardTwoServiceFee));

    Unsigned256 shardOneConvertedCoinA = Unsigned256.create(10);
    Unsigned256 shardOneConvertedGasSumA = Unsigned256.create(100);
    Unsigned256 shardOneConvertedCoinB = Unsigned256.create(5);
    Unsigned256 shardOneConvertedGasSumB = Unsigned256.create(200);

    final FixedList<ConvertedCoin> shardOneConvertedCoins =
        FixedList.create(
            List.of(
                ConvertedCoin.create()
                    .addConvertedCoins(shardOneConvertedCoinA, shardOneConvertedGasSumA),
                ConvertedCoin.create()
                    .addConvertedCoins(shardOneConvertedCoinB, shardOneConvertedGasSumB)));
    context =
        new SysContractContextTest(getBlockProductionTimeForEpochTwo(), 77, blockProducers.get(0));

    ShardId shardOne = shardIds.get(0);
    ShardId shardTwo = shardIds.get(1);
    FeeDistributionContractState threeProducersNotifiedState =
        FeeDistributionContractState.initial(
                bpOrchestrationContract,
                FixedList.create(blockProducers),
                FixedList.create(shardIds))
            .registerEpochEnded(blockProducer, shardOne, epoch, new EpochMetrics(List.of()))
            .registerEpochEnded(blockProducers.get(1), shardOne, epoch, new EpochMetrics(List.of()))
            .registerEpochEnded(blockProducers.get(2), shardOne, epoch, new EpochMetrics(List.of()))
            .registerEpochEnded(blockProducer, shardTwo, epoch, new EpochMetrics(List.of()))
            .registerEpochEnded(blockProducers.get(1), shardTwo, epoch, new EpochMetrics(List.of()))
            .registerEpochEnded(
                blockProducers.get(2), shardTwo, epoch, new EpochMetrics(List.of()));

    Assertions.assertThat(threeProducersNotifiedState.getFeesToDistribute().size()).isEqualTo(0);
    Assertions.assertThat(threeProducersNotifiedState.getExternalCoins().size()).isEqualTo(0);

    Unsigned256 shardTwoConvertedCoin = Unsigned256.ONE;
    Unsigned256 shardTwoConvertedGasSum = Unsigned256.create(5);
    FixedList<ConvertedCoin> shardTwoConvertedCoins =
        FixedList.create(
            List.of(
                ConvertedCoin.create(),
                ConvertedCoin.create()
                    .addConvertedCoins(shardTwoConvertedCoin, shardTwoConvertedGasSum)));

    List<CallbackContext.ExecutionResult> resultsShardTwo =
        List.of(
            createExecutionResult(shardOneConvertedCoins, shardOneUsage, shardOneServiceFees),
            createExecutionResult(shardTwoConvertedCoins, shardTwoUsage, shardTwoServiceFees));
    CallbackContext callbackContext = CallbackContext.create(FixedList.create(resultsShardTwo));
    FeeDistributionContractState storeCollectedFeesShardTwoState =
        serialization.callback(
            context,
            callbackContext,
            threeProducersNotifiedState,
            FeeDistributionContract.Callbacks.collectRewards(
                epoch, FixedList.create(List.of(shardOne, shardTwo))));

    AvlTree<BlockchainAddress, Unsigned256> updatedFeesToDistribute =
        storeCollectedFeesShardTwoState.getFeesToDistribute();
    long notifyingBlockProducers = 3;
    Assertions.assertThat(updatedFeesToDistribute.size()).isEqualTo(notifyingBlockProducers);
    Unsigned256 bpShardOneServiceFeeAfterTax =
        bpShardOneServiceFee.subtract(
            bpShardOneServiceFee.multiply(Unsigned256.create(5)).divide(Unsigned256.create(100)));
    Unsigned256 bpShardTwoServiceFeeAfterTax =
        bpShardTwoServiceFee.subtract(
            bpShardTwoServiceFee.multiply(Unsigned256.create(5)).divide(Unsigned256.create(100)));
    Unsigned256 feesForProducerZero =
        bpShardOneUsage
            .divide(Unsigned256.create(notifyingBlockProducers))
            .add(bpShardTwoUsage.divide(Unsigned256.create(notifyingBlockProducers)))
            .add(Unsigned256.ONE) // Remaining
            .add(bpShardOneServiceFeeAfterTax)
            .add(bpShardTwoServiceFeeAfterTax);
    Assertions.assertThat(updatedFeesToDistribute.getValue(blockProducer))
        .isEqualTo(feesForProducerZero);
    Unsigned256 feesForProducerOne =
        bpShardOneUsage
            .divide(Unsigned256.create(notifyingBlockProducers))
            .add(bpShardTwoUsage.divide(Unsigned256.create(notifyingBlockProducers)));
    Assertions.assertThat(updatedFeesToDistribute.getValue(blockProducers.get(1)))
        .isEqualTo(feesForProducerOne);

    FixedList<ConvertedCoin> updatedExternalCoins =
        storeCollectedFeesShardTwoState.getExternalCoins();
    Assertions.assertThat(updatedExternalCoins).hasSize(2);
    Assertions.assertThat(updatedExternalCoins.get(0).getConvertedCoins())
        .isEqualTo(shardOneConvertedCoinA);
    Assertions.assertThat(updatedExternalCoins.get(0).getConvertedGasSum())
        .isEqualTo(shardOneConvertedGasSumA);
    Assertions.assertThat(updatedExternalCoins.get(1).getConvertedCoins())
        .isEqualTo(shardOneConvertedCoinB.add(shardTwoConvertedCoin));
    Assertions.assertThat(updatedExternalCoins.get(1).getConvertedGasSum())
        .isEqualTo(shardOneConvertedGasSumB.add(shardTwoConvertedGasSum));
  }

  @Test
  void callback_DistributeFeesBasedOnMetrics() {
    long epoch = 1;

    final FeeDistributionContractState initialState =
        FeeDistributionContractState.initial(
            bpOrchestrationContract, FixedList.create(blockProducers), FixedList.create(shardIds));

    final Unsigned256 bpShardUsage = Unsigned256.create(10);
    final Unsigned256 bpServiceFeesProducer0 = Unsigned256.create(50);
    final Unsigned256 bpServiceFeesProducer0AfterTax =
        bpServiceFeesProducer0.subtract(
            bpServiceFeesProducer0.multiply(Unsigned256.create(5)).divide(Unsigned256.create(100)));
    final Unsigned256 bpServiceFeesProducer3 = Unsigned256.create(42);
    final Unsigned256 bpServiceFeesProducer3AfterTax =
        bpServiceFeesProducer3.subtract(
            bpServiceFeesProducer3.multiply(Unsigned256.create(5)).divide(Unsigned256.create(100)));

    final AvlTree<Long, Unsigned256> shardUsage = AvlTree.create(Map.of(epoch, bpShardUsage));
    final AvlTree<BlockchainAddress, Unsigned256> serviceFees =
        AvlTree.create(
            Map.of(
                blockProducers.get(0),
                bpServiceFeesProducer0,
                blockProducers.get(3),
                bpServiceFeesProducer3));

    final FixedList<ConvertedCoin> convertedCoins =
        FixedList.create(
            List.of(
                ConvertedCoin.create(),
                ConvertedCoin.create().addConvertedCoins(Unsigned256.ONE, Unsigned256.create(5))));

    // Producer 0 is one signature away in any of 1, 2 or 3's metrics from being paid. Too bad :(
    FeeDistributionContractState state =
        registerEpochEnded(initialState, 0, List.of(7, 3, 7, 3, 6));
    state = registerEpochEnded(state, 1, List.of(4, 5, 3, 5, 9));
    state = registerEpochEnded(state, 2, List.of(4, 7, 4, 5, 7));
    state = registerEpochEnded(state, 3, List.of(2, 3, 3, 6, 6));

    Assertions.assertThat(state.getFeesToDistribute().size()).isEqualTo(0);
    Assertions.assertThat(state.getExternalCoins().size()).isEqualTo(0);

    CallbackContext callbackContext =
        CallbackContext.create(
            FixedList.create(
                List.of(createExecutionResult(convertedCoins, shardUsage, serviceFees))));
    state =
        serialization.callback(
            context,
            callbackContext,
            state,
            FeeDistributionContract.Callbacks.collectRewards(
                epoch, FixedList.create(List.of(shardIds.get(0)))));

    AvlTree<BlockchainAddress, Unsigned256> feesToDistribute = state.getFeesToDistribute();
    Assertions.assertThat(feesToDistribute.size()).isEqualTo(4);

    // producer 0 only gets corresponding to their service fees because they were not deemed active
    Assertions.assertThat(feesToDistribute.getValue(blockProducers.get(0)))
        .isEqualTo(bpServiceFeesProducer0AfterTax);

    // producer 1 gets bpShardUsage / 3 + 1 since (1) there are only 3 good producers (the 4 who
    // notified minus producer 0 who nobody liked) + 1 because 10 isn't divisible by 3.
    Assertions.assertThat(feesToDistribute.getValue(blockProducers.get(1)))
        .isEqualTo(bpShardUsage.divide(Unsigned256.create(3)).add(Unsigned256.ONE));

    // producer 2 and 3 each get a third of the shard usage. Producer 3 also has some service fees
    // that they receive.
    Assertions.assertThat(feesToDistribute.getValue(blockProducers.get(2)))
        .isEqualTo(bpShardUsage.divide(Unsigned256.create(3)));
    Assertions.assertThat(feesToDistribute.getValue(blockProducers.get(3)))
        .isEqualTo(bpShardUsage.divide(Unsigned256.create(3)).add(bpServiceFeesProducer3AfterTax));
  }

  private FeeDistributionContractState registerEpochEnded(
      FeeDistributionContractState state, int bpIndex, List<Integer> metrics) {
    return state.registerEpochEnded(
        blockProducers.get(bpIndex), shardIds.get(0), 1L, metrics(metrics));
  }

  private static EpochMetrics metrics(List<Integer> frequencies) {
    return new EpochMetrics(frequencies);
  }

  @Test
  void epochInformationForNullShard() {
    EpochInformation initial = EpochInformation.initial();
    FixedList<BlockchainAddress> notifyingBlockProducersForShard =
        initial.getBlockProducersForShardBasedOnMetrics(
            shardIds.get(0), FixedList.create(blockProducers));
    Assertions.assertThat(notifyingBlockProducersForShard).isNull();
  }

  @Test
  public void callback_OnlySomeShardsWithNotify() {
    long epoch = 1;

    BlockchainAddress blockProducer = blockProducers.get(0);

    Unsigned256 bpShardOneUsage = Unsigned256.create(100);

    final AvlTree<Long, Unsigned256> shardOneUsage = AvlTree.create(Map.of(epoch, bpShardOneUsage));

    context =
        new SysContractContextTest(getBlockProductionTimeForEpochTwo(), 77, blockProducers.get(0));

    ShardId shardOne = shardIds.get(0);
    ShardId shardTwo = shardIds.get(1);
    FeeDistributionContractState threeProducersNotifiedState =
        FeeDistributionContractState.initial(
                bpOrchestrationContract,
                FixedList.create(blockProducers),
                FixedList.create(shardIds))
            .registerEpochEnded(blockProducer, shardTwo, epoch, new EpochMetrics(List.of()));

    List<CallbackContext.ExecutionResult> resultsShardTwo =
        List.of(
            createExecutionResult(FixedList.create(), shardOneUsage, AvlTree.create()),
            createExecutionResult(FixedList.create(), AvlTree.create(), AvlTree.create()));
    CallbackContext callbackContext = CallbackContext.create(FixedList.create(resultsShardTwo));
    FeeDistributionContractState storeCollectedFeesShardTwoState =
        serialization.callback(
            context,
            callbackContext,
            threeProducersNotifiedState,
            FeeDistributionContract.Callbacks.collectRewards(
                epoch, FixedList.create(List.of(shardOne, shardTwo))));
    Assertions.assertThat(storeCollectedFeesShardTwoState.getFeesToDistribute().size())
        .isEqualTo(5);
  }

  @Test
  public void gasRewardPoolCreateFromStateAccessorDoesNotCreateNull() {
    FeeDistributionContractState initial =
        new FeeDistributionContractState(
            bpOrchestrationContract,
            FixedList.create(blockProducers),
            FixedList.create(),
            AvlTree.create(),
            AvlTree.create(),
            0L,
            FixedList.create(shardIds),
            Signed256.create());
    FeeDistributionContract contract = new FeeDistributionContract();
    StateAccessor accessor = StateAccessor.create(initial);
    FeeDistributionContractState updated = contract.upgrade(accessor);
    Assertions.assertThat(updated.getGasRewardPool()).isNotNull();
  }

  @Test
  public void callbackInvocationDoesNotExist() {
    FeeDistributionContractState initial =
        FeeDistributionContractState.initial(
            bpOrchestrationContract, FixedList.create(blockProducers), FixedList.create());
    long epoch = 1;
    CallbackContext callbackContext = CallbackContext.create(FixedList.create());
    byte invocationByte = 127;
    Assertions.assertThatThrownBy(
            () ->
                serialization.callback(
                    context,
                    callbackContext,
                    initial,
                    rpc -> {
                      rpc.writeByte(invocationByte);
                      rpc.writeLong(epoch);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Invalid shortname: 127");
  }

  static CallbackContext.ExecutionResult createExecutionResult(
      FixedList<ConvertedCoin> convertedCoins,
      AvlTree<Long, Unsigned256> epochUsageTree,
      AvlTree<BlockchainAddress, Unsigned256> serviceFees) {
    return CallbackContext.createResult(
        null,
        true,
        SafeDataInputStream.createFromBytes(
            SafeDataOutputStream.serialize(
                stream -> {
                  stream.writeLong(convertedCoins.size());
                  for (ConvertedCoin convertedCoin : convertedCoins) {
                    convertedCoin.getConvertedCoins().write(stream);
                    convertedCoin.getConvertedGasSum().write(stream);
                  }

                  stream.writeLong(epochUsageTree.size());
                  for (Long epoch : epochUsageTree.keySet()) {
                    stream.writeLong(epoch);
                    epochUsageTree.getValue(epoch).write(stream);
                  }

                  Set<BlockchainAddress> producersWithFees = serviceFees.keySet();
                  stream.writeLong(producersWithFees.size());
                  for (BlockchainAddress blockchainAddress : producersWithFees) {
                    blockchainAddress.write(stream);
                    serviceFees.getValue(blockchainAddress).write(stream);
                  }
                  Unsigned256 infrastructureFeeSum = Unsigned256.TEN;
                  infrastructureFeeSum.write(stream);
                })));
  }

  @Immutable
  static final class GlobalState implements StateSerializable {

    @SuppressWarnings("Immutable")
    ExternalCoins coins;

    GlobalState() {
      this.coins = new ExternalCoins(FixedList.create());
    }

    void setCoinSymbols(List<String> symbols) {
      this.coins = new ExternalCoins(FixedList.create(symbols.stream().map(Coin::new)));
    }
  }

  @Immutable
  private static final class ExternalCoins implements StateSerializable {

    @SuppressWarnings("unused")
    final FixedList<Coin> coins;

    private ExternalCoins(FixedList<Coin> coins) {
      this.coins = coins;
    }
  }

  @Immutable
  private static final class Coin implements StateSerializable {

    @SuppressWarnings("unused")
    final String symbol;

    private Coin(String symbol) {
      this.symbol = symbol;
    }
  }

  /**
   * Calculates <var>x</var> in <code>x - floor(x / 100) * 5 = PAYOUT_THRESHOLD</code> (with integer
   * division) to iteratively find the exact value to add for reaching the threshold after a 5% tax
   * has been deducted.
   *
   * @return x
   */
  private Unsigned256 calculateTaxForThreshold(Unsigned256 threshold) {
    Unsigned256 tax = Unsigned256.ZERO;
    Unsigned256 hundred = Unsigned256.create(100);
    Unsigned256 five = Unsigned256.create(5);
    for (int i = 0; i < 10; i++) {
      tax = threshold.add(tax).multiply(five).divide(hundred);
    }
    return tax;
  }
}
