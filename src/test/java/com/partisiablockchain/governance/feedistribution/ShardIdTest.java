package com.partisiablockchain.governance.feedistribution;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import nl.jqno.equalsverifier.EqualsVerifier;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ShardIdTest {

  @Test
  public void equals() {
    String shardA = "ShardA";
    String shardB = "ShardB";
    EqualsVerifier.simple()
        .forClass(ShardId.class)
        .withPrefabValues(String.class, shardA, shardB)
        .verify();
  }

  @Test
  public void compareTo() {
    ShardId nullShard = new ShardId(null);
    ShardId shardIdA = new ShardId("ShardA");
    ShardId shardIdB = new ShardId("ShardB");
    Assertions.assertThat(nullShard.compareTo(shardIdA)).isEqualTo(-1);
    Assertions.assertThat(shardIdA.compareTo(nullShard)).isEqualTo(1);
    Assertions.assertThat(shardIdA.compareTo(shardIdB)).isEqualTo(-1);
    Assertions.assertThat(shardIdB.compareTo(shardIdA)).isEqualTo(1);
  }
}
