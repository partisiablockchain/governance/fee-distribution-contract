package com.partisiablockchain.governance.feedistribution;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.serialization.StateAccessor;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class EpochMetricsTest {

  private final List<Integer> signatureFrequencies = List.of(1, 5, 3, 4, 2, 6, 5, 5);
  private final EpochMetrics epochMetrics = new EpochMetrics(signatureFrequencies);

  @Test
  void goodIndices() {
    List<Integer> goodIndices = epochMetrics.getGoodIndices(2);
    Assertions.assertThat(goodIndices).containsExactlyInAnyOrder(1, 3, 5, 6, 7);
    Assertions.assertThat(epochMetrics.getGoodIndices(4)).hasSize(0);
  }

  @Test
  void fromStateAccessor() {
    StateAccessor accessor = StateAccessor.create(epochMetrics);
    EpochMetrics fromStateAccessor = EpochMetrics.createFromStateAccessor(accessor);
    Assertions.assertThat(fromStateAccessor.getSignatureFrequencies())
        .containsExactlyElementsOf(FixedList.create(signatureFrequencies));
  }
}
