package com.partisiablockchain.governance.feedistribution;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ShardInformationTest {

  private static FixedList<BlockchainAddress> blockProducers =
      FixedList.create(
          List.of(
              address(0),
              address(1),
              address(2),
              address(3),
              address(4),
              address(5),
              address(6),
              address(7)));

  @Test
  void metricsConsolidation() {
    ShardInformation info = ShardInformation.initial();
    //                                                           0  1  2  3  4  5  6  7
    info = info.registerEpochEnded(address(1), metrics(List.of(1, 0, 1, 1, 1, 1, 1, 1)));
    info = info.registerEpochEnded(address(2), metrics(List.of(1, 0, 1, 1, 1, 1, 1, 1)));
    info = info.registerEpochEnded(address(3), metrics(List.of(1, 0, 1, 0, 0, 1, 1, 1)));
    info = info.registerEpochEnded(address(4), metrics(List.of(1, 1, 1, 0, 0, 1, 1, 1)));
    info = info.registerEpochEnded(address(5), metrics(List.of(1, 1, 1, 1, 0, 1, 1, 1)));

    // 0, 6 and 7 look good, but didn't notify so won't get included
    // 1 and 4 only look good towards 2 producers, so won't get included
    // 3 look good towards t+1 producers and so gets included
    // 2 and 5 look good towards more than t+1 producers and so gets included

    FixedList<BlockchainAddress> consolidatedMetrics = info.filterByReportedMetrics(blockProducers);
    Assertions.assertThat(consolidatedMetrics)
        .containsExactlyInAnyOrder(
            blockProducers.get(3), blockProducers.get(2), blockProducers.get(5));

    // Test of filtering when n is not exactly equal to 3t + 1, e.g. n = 6
    blockProducers = blockProducers.subList(0, 6);
    info = ShardInformation.initial();
    //                                                           0  1  2  3  4  5
    info = info.registerEpochEnded(address(0), metrics(List.of(1, 1, 1, 1, 0, 0)));
    info = info.registerEpochEnded(address(1), metrics(List.of(1, 1, 1, 1, 0, 0)));
    info = info.registerEpochEnded(address(2), metrics(List.of(1, 1, 1, 1, 0, 0)));
    info = info.registerEpochEnded(address(3), metrics(List.of(1, 1, 1, 1, 0, 0)));
    info = info.registerEpochEnded(address(4), metrics(List.of(1, 1, 1, 1, 0, 0)));
    info = info.registerEpochEnded(address(5), metrics(List.of(1, 1, 1, 1, 0, 0)));

    // 4 and 5 are not signing, so won't get included
    // 0, 1, 2 and 3 look good and so gets included

    consolidatedMetrics = info.filterByReportedMetrics(blockProducers);
    Assertions.assertThat(consolidatedMetrics)
        .containsExactlyInAnyOrder(
            blockProducers.get(0),
            blockProducers.get(1),
            blockProducers.get(2),
            blockProducers.get(3));
  }

  private static BlockchainAddress address(int i) {
    return BlockchainAddress.fromString(String.format("%042d", i));
  }

  private static EpochMetrics metrics(List<Integer> frequencies) {
    return new EpochMetrics(frequencies);
  }

  @Test
  void metricsNotIncrementedForInvalidIndex() {
    ShardInformation info = ShardInformation.initial();
    info = info.registerEpochEnded(address(1), metrics(List.of(1, 2)));
    FixedList<BlockchainAddress> truncated = blockProducers.subList(0, 1);
    FixedList<BlockchainAddress> blockchainAddresses = info.filterByReportedMetrics(truncated);
    Assertions.assertThat(blockchainAddresses).hasSize(1);
  }
}
